"""Webhook interaction tests."""
from unittest import TestCase
from unittest import mock

from webhook import rh_metadata

MOCK_DATA = {'projects': [{'name': 'project1',
                           'ids': [12345, 56789],
                           'inactive': False,
                           'product': 'Project 1',
                           'branches': [{'name': 'main',
                                         'inactive': False,
                                         'component': 'kernel',
                                         'distgit_ref': 'rhel-10.1.0',
                                         'internal_target_release': '10.1.0'
                                         },
                                        {'name': '10.0',
                                         'component': 'kernel-rt',
                                         'distgit_ref': 'rhel-10.0.0',
                                         'zstream_target_release': '10.0.0'
                                         },
                                        {'name': '9.9',
                                         'component': 'kernel-auto',
                                         'distgit_ref': 'rhel-9.9.0',
                                         'zstream_target_release': '9.9.0'
                                         },
                                        {'name': '9.8',
                                         'inactive': True,
                                         'component': 'kernel-auto',
                                         'distgit_ref': 'rhel-9.8.0',
                                         'zstream_target_release': '9.8.0'
                                         }]
                           }]
             }


class TestRHMetadata(TestCase):
    """Tests for the projects module."""

    @mock.patch('webhook.rh_metadata.load')
    @mock.patch('webhook.rh_metadata.check_data', wraps=rh_metadata.check_data)
    def test_Projects(self, mock_check_data, mock_projects_yaml):
        """Test Projects loading."""
        mock_projects_yaml.return_value = MOCK_DATA
        results = rh_metadata.Projects()
        rh_metadata.check_data.assert_called()
        self.assertTrue(results.__dataclass_params__.frozen)

        self.assertEqual(len(results.projects), 1)
        self.assertEqual(len(results.projects[0].branches), 4)

        self.assertEqual(results.get_project_by_id(54321), None)
        self.assertEqual(results.get_project_by_id(12345).product, 'Project 1')

        self.assertEqual(results.get_project_by_name('project2'), None)
        self.assertEqual(results.get_project_by_name('project1').name, 'project1')

    @mock.patch('webhook.rh_metadata.load')
    @mock.patch('webhook.rh_metadata.get_policy_data')
    def test_Projects_policies_loading(self, mock_get_policy_data, mock_projects_yaml):
        """Calls load_policies method."""
        mock_get_policy_data.return_value = {'rhel-10.1.0': [1, 2, 3],
                                             'rhel-10.0.0': None,
                                             'rhel-9.9.0': None,
                                             'rhel-9.8.0': [4, 5, 6]
                                             }
        mock_projects_yaml.return_value = MOCK_DATA
        results = rh_metadata.Projects(load_policies=True)
        self.assertEqual(results.projects[0].branches[0].policy, [1, 2, 3])
        self.assertEqual(results.projects[0].branches[1].policy, [])
        self.assertEqual(results.projects[0].branches[2].policy, [])
        self.assertEqual(results.projects[0].branches[3].policy, [4, 5, 6])

    @mock.patch('webhook.rh_metadata.load')
    @mock.patch('webhook.rh_metadata.check_data', wraps=rh_metadata.check_data)
    def test_Project(self, mock_check_data, mock_projects_yaml):
        """Test Project objects."""
        mock_projects_yaml.return_value = MOCK_DATA
        results = rh_metadata.Projects()
        rh_metadata.check_data.assert_called()

        project = results.projects[0]
        self.assertTrue(project.__dataclass_params__.frozen)

        self.assertEqual(project.get_branch_by_name('1'), None)
        self.assertEqual(project.get_branch_by_name('main').distgit_ref, 'rhel-10.1.0')

        self.assertEqual(project.get_branches_by_itr('1.0'), [])
        self.assertEqual(project.get_branches_by_itr('10.1.0')[0].component, 'kernel')

        self.assertEqual(project.get_branches_by_ztr('10.1.0'), [])
        self.assertEqual(project.get_branches_by_ztr('9.9.0')[0].component, 'kernel-auto')

        # branch is inactive so an empty list
        self.assertEqual(project.get_branches_by_ztr('9.8.0'), [])

    @mock.patch('webhook.rh_metadata.check_data', wraps=rh_metadata.check_data)
    def test_Branch(self, mock_check_data):
        """Test Branch objects."""
        branch = rh_metadata.Branch('test_branch', 'kernel-rt', 'rhel-2.5.5', '2.5.5')
        rh_metadata.check_data.assert_called()

        self.assertTrue(branch.__dataclass_params__.frozen)
        self.assertEqual(branch.name, 'test_branch')
        self.assertEqual(branch.component, 'kernel-rt')
        self.assertEqual(branch.distgit_ref, 'rhel-2.5.5')
        self.assertEqual(branch.internal_target_release, '2.5.5')
        self.assertEqual(branch.zstream_target_release, '')
        self.assertEqual(branch.inactive, False)

        # set_policy method just assigns a list.
        a_list = [1, 2, 3]
        branch.set_policy(a_list)
        self.assertEqual(branch.policy, a_list)

    def test_check_data(self):
        """Make sure the expected errors are raised."""
        # Wrong field type
        branch = rh_metadata.Branch('test_branch', 'kernel-rt', 'rhel-2.5.5')
        branch.__dict__['distgit_ref'] = 5
        err_raised = False
        try:
            rh_metadata.check_data(branch)
        except TypeError:
            err_raised = True
        self.assertTrue(err_raised)

        # Empty string for value with no default
        branch = rh_metadata.Branch('test_branch', 'kernel-rt', 'rhel-2.5.5')
        branch.__dict__['component'] = ''
        err_raised = False
        try:
            rh_metadata.check_data(branch)
        except ValueError:
            err_raised = True
        self.assertTrue(err_raised)

        # Empty list for value with no default
        branch = {'name': 'branch', 'component': 'kernel', 'distgit_ref': 'rhel-1.2.3'}
        project = rh_metadata.Project([123, 456], 'project', 'rhel', [branch])
        project.__dict__['branches'] = []
        err_raised = False
        try:
            rh_metadata.check_data(project)
        except ValueError:
            err_raised = True
        self.assertTrue(err_raised)

        # happy branch, empty default values don't raise a ValueError
        branch = rh_metadata.Branch('test_branch', 'kernel-rt', 'rhel-2.5.5', '2.5.5')
        self.assertEqual(branch.internal_target_release, '2.5.5')
        self.assertEqual(branch.zstream_target_release, '')

    @mock.patch('webhook.rh_metadata.load')
    def test_is_branch_active(self, mock_load):
        """Returns True if the branch exists and is not marked inactive in rh_metadata, or False."""
        mock_load.return_value = MOCK_DATA
        self.assertTrue(rh_metadata.is_branch_active(12345, 'main'))
        self.assertTrue(rh_metadata.is_branch_active(56789, '10.0'))
        self.assertTrue(rh_metadata.is_branch_active(12345, '9.9'))
        self.assertFalse(rh_metadata.is_branch_active(12345, '9.8'))
        self.assertFalse(rh_metadata.is_branch_active(99999, 'main'))
        self.assertTrue(rh_metadata.is_branch_active('project1', 'main'))
        self.assertTrue(rh_metadata.is_branch_active('12345', '9.9'))
