"""Tests for graphql module."""
import unittest
from unittest import mock

from gql import gql
from gql.transport.exceptions import TransportQueryError

from webhook import graphql


class TestHelpers(unittest.TestCase):
    """Test helper functions."""

    @mock.patch('webhook.graphql._check_user', wraps=graphql._check_user)
    @mock.patch('webhook.graphql._check_keys', wraps=graphql._check_keys)
    def test_check_query_results(self, mock_check_keys, mock_check_user):
        """Test check_query_results."""
        # nothing to do
        mock_results = {}
        self.assertTrue(graphql.check_query_results(mock_results, None, None) is mock_results)
        mock_check_user.assert_not_called()
        mock_check_keys.assert_not_called()

        # check user and it doesn´t match
        mock_results = {'currentUser': {'username': 'cool_guy'}}
        self.assertTrue(graphql.check_query_results(mock_results, None, 'steve') is mock_results)
        mock_check_user.assert_called_with(mock_results, 'steve')
        mock_check_keys.assert_not_called()

        # check user matches
        mock_check_user.reset_mock()
        self.assertEqual(graphql.check_query_results(mock_results, None, 'cool_guy'), None)
        mock_check_user.assert_called_with(mock_results, 'cool_guy')
        mock_check_keys.assert_not_called()

        # check keys and they are all there
        mock_check_user.reset_mock()
        mock_results = {'users': {}, 'groups': {}}
        self.assertTrue(graphql.check_query_results(mock_results, {'users'}, None) is mock_results)
        mock_check_user.assert_not_called()
        mock_check_keys.assert_called_with(mock_results, {'users'})

        # check keys and they are not all there
        mock_check_keys.reset_mock()
        mock_results = {'users': {}, 'groups': {}}
        err_raised = False
        try:
            graphql.check_query_results(mock_results, {'fans', 'users'}, None)
        except RuntimeError:
            err_raised = True
        self.assertTrue(err_raised)
        mock_check_user.assert_not_called()
        mock_check_keys.assert_called_with(mock_results, {'fans', 'users'})


class TestGitlabGraph(unittest.TestCase):
    """Test GitlabGraph methods."""

    @mock.patch('webhook.graphql.get_session')
    @mock.patch('webhook.graphql.get_token')
    @mock.patch('webhook.graphql.Client')
    def test_init(self, mock_client, mock_get_token, mock_get_session):
        """Test a new object sets up the client."""
        mock_get_session.return_value = mock.Mock(headers={})
        mock_get_token.return_value = 'fake_token'
        mygraph = graphql.GitlabGraph()
        transport = mock_client.call_args.kwargs['transport']
        self.assertEqual(transport.__class__.__name__, '_CkiRequestsHTTPTransport')
        self.assertEqual(transport.session, mock_get_session())
        self.assertEqual(transport.session.headers, {'Authorization': 'Bearer fake_token'})
        self.assertEqual(mygraph.client, mock_client())

    @mock.patch('webhook.graphql.check_query_results')
    @mock.patch('webhook.graphql.Client')
    def test_execute_query(self, mock_client, mock_check):
        """Test the execute_query method."""
        query = gql('{currentUser {username}}')
        mygraph = graphql.GitlabGraph()
        result = mygraph.execute_query(query)
        mygraph.client.execute.assert_called_with(query, variable_values=None)
        mock_check.assert_called_with(mock_client().execute.return_value, None, None)
        self.assertEqual(result, mock_check.return_value)

    @mock.patch('webhook.graphql.check_query_results')
    @mock.patch('webhook.graphql.Client')
    def test_execute_query_error(self, mock_client, mock_check):
        """Test execute_query raises the expected error."""
        query = gql('{currentUser1 {username}}')
        mygraph = graphql.GitlabGraph()
        mygraph.client.execute.side_effect = \
            TransportQueryError("Encountered 1 error(s) executing query: {currentUser1 {username}}",
                                errors=["Field 'currentUser1' doesn't exist on type 'Query'"])

        exception_hit = False
        try:
            mygraph.execute_query(query)
        except TransportQueryError:
            exception_hit = True
        self.assertTrue(exception_hit)
        mock_check.assert_not_called()

    @mock.patch('webhook.graphql.Client')
    def test_execute_paged_query(self, mock_client):
        """Test execute_paged_query returns the expected results."""
        mygraph = graphql.GitlabGraph()
        mygraph.execute_query = mock.Mock()

        query = 'mock_query'
        page_key = ['project', 'mergeRequest', 'commits']
        params = {'namespace': 'group/project', 'mr_id': 123}
        check_keys = {'project'}
        check_user = 'testuser'

        # No results returns None
        mygraph.execute_query.return_value = None
        result = mygraph.execute_paged_query(query, page_key, params=params, check_keys=check_keys,
                                             check_user=check_user)
        self.assertEqual(result, None)
        expected_params = dict(params, **{'cursor': '', 'first_pass': True})
        mygraph.execute_query.assert_called_once_with(query, params=expected_params,
                                                      check_keys=check_keys, check_user=check_user)

        # Some results
        mygraph.execute_query.reset_mock(return_value=True)

        # Expected results of execute_query
        commits1 = {'pageInfo': {'hasNextPage': True, 'endCursor': 'Abc'}, 'nodes': [1, 2, 3]}
        commits2 = {'pageInfo': {'hasNextPage': True, 'endCursor': 'Def'}, 'nodes': [4, 5, 6]}
        commits3 = {'pageInfo': {'hasNextPage': False, 'endCursor': 'Ghi'}, 'nodes': [7, 8]}
        result1 = {'project': {'mergeRequest': {'commits': commits1, 'desciption': 'hey'}}}
        result2 = {'project': {'mergeRequest': {'commits': commits2}}}
        result3 = {'project': {'mergeRequest': {'commits': commits3}}}
        mygraph.execute_query.side_effect = [result1, result2, result3]

        result = mygraph.execute_paged_query(query, page_key, params=params, check_keys=check_keys,
                                             check_user=check_user)
        # Expected return of execute_paged_query is the first result updated with all node values
        commits1['pageInfo']['nodes'] = [1, 2, 3, 4, 5, 6, 7, 8]
        expected = {'project': {'mergeRequest': {'commits': commits1, 'desciption': 'hey'}}}
        self.assertEqual(result, expected)

        # execute_query should be called thrice with updated cursor and None check_* values
        self.assertEqual(mygraph.execute_query.call_count, 3)
        call1 = mock.call(query, params=expected_params, check_keys=check_keys,
                          check_user=check_user)
        expected_params['cursor'] = 'Abc'
        expected_params['first_pass'] = False
        call2 = mock.call(query, params=expected_params, check_keys=None, check_user=None)
        expected_params['cursor'] = 'Def'
        call3 = mock.call(query, params=expected_params, check_keys=None, check_user=None)
        mygraph.execute_query.assert_has_calls([call1, call2, call3])

    @mock.patch('webhook.graphql.Client')
    def test_create_note(self, mock_client):
        """Test create_note."""
        mygraph = graphql.GitlabGraph()
        mygraph.execute_query = mock.Mock()

        # Not production.
        with mock.patch('webhook.graphql.is_production', return_value=False):
            result = mygraph.create_note(123, 'hello')
            self.assertTrue(result)
            mygraph.execute_query.assert_not_called()

        # Production.
        mygraph.execute_query.return_value = {'createNote': {'note': {'id': 456}}}
        with mock.patch('webhook.graphql.is_production', return_value=True):
            result = mygraph.create_note(123, 'hello')
            self.assertEqual(result, 456)

        # Production with no result.
        mygraph.execute_query.return_value = None
        exception_hit = False
        with mock.patch('webhook.graphql.is_production', return_value=True):
            try:
                mygraph.create_note(123, 'hello')
            except RuntimeError:
                exception_hit = True
            self.assertTrue(exception_hit)
