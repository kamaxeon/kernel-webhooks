"""Webhook interaction tests."""
from json import load
from unittest import TestCase
from unittest import mock

from webhook import gitbz


class TestGBZResponse(TestCase):
    """Test GBZResponse dataclass."""

    def test_bad_status(self):
        """Make sure we blow up if status field is not of type gitbz.Status."""
        err_raised = False
        try:
            gitbz.GBZResponse(123, None, 'hello')
        except TypeError:
            err_raised = True
        self.assertTrue(err_raised)

    def test_frozen(self):
        """Object should be frozen."""
        result = gitbz.GBZResponse(123, gitbz.Status.APPROVED, '')
        self.assertTrue(result.__dataclass_params__.frozen)

    def test_process_details_not_invalid(self):
        """For non-Invalid Status do nothing."""
        with mock.patch('webhook.gitbz.xmlrpc_fault', wrap=gitbz.xmlrpc_fault) as mock_fault:
            gitbz.GBZResponse(123, gitbz.Status.APPROVED, '')
            mock_fault.assert_not_called()

    def test_process_details_xmlrpc(self):
        """Digs out the xml error code and message and updates the object."""
        details = 'error: XMLRPC Fault #101 from Bugzilla: Bug #99999999 does not exist.'
        result = gitbz.GBZResponse(99999999, gitbz.Status.INVALID, details)
        self.assertTrue(result.status is gitbz.Status.INVALID)
        self.assertEqual(result.error, 101)
        self.assertEqual(result.details, 'Bug #99999999 does not exist.')

    def test_process_details_504(self):
        """Identifies the error code and updates the object with Status."""
        details = 'error: <ProtocolError for bugzilla.redhat.com/xmlrpc.cgi: 504 Gateway Time-out>'
        result = gitbz.GBZResponse(123, gitbz.Status.INVALID, details)
        self.assertTrue(result.status is gitbz.Status.TEMP_FAILURE)
        self.assertEqual(result.error, 504)
        self.assertEqual(result.details,
                         'ProtocolError for bugzilla.redhat.com/xmlrpc.cgi: 504 Gateway Time-out')

    def test_process_details_dns(self):
        """Identifies the error code and updates the object with Status."""
        details = 'error: [Errno -3] Temporary failure in name resolution'
        result = gitbz.GBZResponse(123, gitbz.Status.INVALID, details)
        self.assertTrue(result.status is gitbz.Status.TEMP_FAILURE)
        self.assertEqual(result.error, -3)
        self.assertEqual(result.details, '[Errno -3] Temporary failure in name resolution')

    def test_process_details_unknown_invalid(self):
        """Sets error field to -1."""
        result = gitbz.GBZResponse(123, gitbz.Status.INVALID, 'just some random error')
        self.assertTrue(result.status is gitbz.Status.INVALID)
        self.assertEqual(result.error, -1)
        self.assertEqual(result.details, 'just some random error')

    def test_update_response(self):
        """Takes new status/details and updates the object appropriately."""
        result = gitbz.GBZResponse(123, gitbz.Status.TODO, '', error=123)
        result.__dict__['_process_details'] = mock.Mock()
        result.update_response(gitbz.Status.UNAPPROVED, 'oh no')
        self.assertTrue(result.status is gitbz.Status.UNAPPROVED)
        self.assertEqual(result.details, 'oh no')
        self.assertEqual(result.error, 0)
        result._process_details.assert_called_once()


class TestHelpers(TestCase):
    """Test the various helper functions."""

    def test_get_gitbz_session(self):
        with mock.patch('webhook.gitbz.get_session') as mock_get_session:
            gitbz.get_gitbz_session()
            mock_get_session.assert_called_once()

    def test_xmlrpc_fault(self):
        """Confirm we return the expected tuples."""
        # No match, return an empty list
        self.assertEqual(gitbz.xmlrpc_fault('a nonsense string'), ())

        # some match.
        string = 'error: XMLRPC Fault #101 from Bugzilla: Bug #99999999 does not exist.'
        self.assertEqual(gitbz.xmlrpc_fault(string), (101, 'Bug #99999999 does not exist.'))

    @mock.patch('webhook.gitbz.GBZQuery')
    def test_query_gitbz(self, mock_gbzquery):
        """Ensure the wrapper calls run() and returns the bug_list."""
        mock_querier = mock.Mock()
        mock_gbzquery.return_value = mock_querier
        result = gitbz.query('branch', [123, 456])
        mock_gbzquery.assert_called_once_with('branch', [123, 456])
        mock_querier.run.assert_called_once()
        self.assertEqual(mock_querier.bug_list, result)


class TestGBZQuery(TestCase):
    """Ensure the object works as expected."""

    @mock.patch('webhook.gitbz.get_gitbz_session')
    def test_init(self, mock_get_session):
        """Session should be set and bug_list transformed into TODO Status GBZResponse objects."""
        result = gitbz.GBZQuery('branch', [123, 456])
        mock_get_session.assert_called_once()
        self.assertEqual(result.branch, 'branch')
        self.assertEqual(len(result.bug_list), 2)
        self.assertEqual(result.bug_list[0].id, 123)
        self.assertEqual(result.bug_list[0].status, gitbz.Status.TODO)
        self.assertEqual(result.bug_list[1].id, 456)
        self.assertEqual(result.bug_list[1].status, gitbz.Status.TODO)

    @mock.patch('webhook.gitbz.get_gitbz_session')
    def test_run(self, mock_get_session):
        """Gets bugs to query, calls _run, and decrements retries."""
        with mock.patch('json.dumps'):
            mock_branch = mock.Mock(component='kernel', distgit_ref='rhel-8.4.0')
            mock_branch.name = '8.4'
            result = gitbz.GBZQuery(mock_branch, [123, 456, 789])
            bugs_to_query1 = [result.bug_list[0], result.bug_list[1], result.bug_list[2]]
            bugs_to_query2 = [result.bug_list[1]]
            result._bugs_to_query = mock.Mock()
            result._bugs_to_query.side_effect = [bugs_to_query1, bugs_to_query2, bugs_to_query2, []]
            result._mark_bugs_missing = mock.Mock()
            result._update_bugs = mock.Mock()
            result.run()

            self.assertEqual(result._bugs_to_query.call_count, 4)
            self.assertEqual(result._mark_bugs_missing.call_count, 3)
            self.assertEqual(result._update_bugs.call_count, 3)

    def test_update_bugs(self):
        """Triggers calls to update_bug method with the expected input."""
        result = gitbz.GBZQuery('branch', [123])
        result.update_bug = mock.Mock()

        with open('tests/gitbz-test.json') as json_file:
            mock_json = load(json_file)
        result._update_bugs(mock_json['logs'])
        self.assertEqual(result.update_bug.call_count, 4)
        self.assertEqual(result.update_bug.mock_calls[0].args[:2],
                         ('2034863', gitbz.Status.APPROVED))
        self.assertEqual(result.update_bug.mock_calls[1].args[:2],
                         ('2028533', gitbz.Status.APPROVED))
        self.assertEqual(result.update_bug.mock_calls[2].args[:2],
                         ('2036932', gitbz.Status.UNAPPROVED))
        self.assertEqual(result.update_bug.mock_calls[3].args[:2],
                         ('99999999', gitbz.Status.INVALID))

    def test_bugs_to_query(self):
        """Return any TODO or TEMP_FAILURE items when there are retries."""
        result = gitbz.GBZQuery('branch', [123, 456, 789, 147, 258, 369])
        result._retries = 2
        result.bug_list[0].__dict__['status'] = gitbz.Status.APPROVED
        result.bug_list[2].__dict__['status'] = gitbz.Status.UNAPPROVED
        result.bug_list[4].__dict__['status'] = gitbz.Status.TEMP_FAILURE
        result.bug_list[5].__dict__['status'] = gitbz.Status.INVALID
        expected = [result.bug_list[1], result.bug_list[3], result.bug_list[4]]
        self.assertEqual(result._bugs_to_query(), expected)

        # no retries, expect an empty list
        result._retries = -1
        self.assertEqual(result._bugs_to_query(), [])

    def test_mark_bugs_missing(self):
        """Calls update_bug against every bug in the list."""
        result = gitbz.GBZQuery('branch', [123, 456, 789])
        result.update_bug = mock.Mock()
        result._mark_bugs_missing(result.bug_list)
        self.assertEqual(result.update_bug.call_count, 3)
        mstr = 'No results found for this BZ.'
        self.assertEqual(result.update_bug.call_args_list[0],
                         mock.call(123, gitbz.Status.MISSING, mstr))
        self.assertEqual(result.update_bug.call_args_list[1],
                         mock.call(456, gitbz.Status.MISSING, mstr))
        self.assertEqual(result.update_bug.call_args_list[2],
                         mock.call(789, gitbz.Status.MISSING, mstr))

    def test_update_bug(self):
        """Find the bug in the bug list and call its update_response method."""
        result = gitbz.GBZQuery('branch', [123, 456, 789])
        result.bug_list[1].__dict__['update_response'] = \
            mock.Mock(wrap=gitbz.GBZResponse.update_response)
        result.update_bug(456, gitbz.Status.APPROVED, 'good job')
        result.bug_list[1].update_response.assert_called_once_with(gitbz.Status.APPROVED,
                                                                   'good job')

        # Blow up if the bug isn't in our list
        err_raised = False
        try:
            result.update_bug(999, gitbz.Status.APPROVED, 'good job')
        except RuntimeError:
            err_raised = True
        self.assertTrue(err_raised)
