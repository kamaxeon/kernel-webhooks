"""Query bugzilla for MRs."""
import enum
import re
import sys
from xmlrpc.client import Fault

from cki_lib import logger
from gitlab.const import MAINTAINER_ACCESS

from webhook.cpc import get_one_policy
from webhook.cpc import is_bug_ready
from webhook.rh_metadata import Projects

from . import cdlib
from . import common
from . import defs

LOGGER = logger.get_logger('cki.webhook.bugzilla')


def nobug_msg():  # pragma: no cover
    """Return the message to use as a note for commits without bz."""
    msg = "No 'Bugzilla:' entry was found in these commits.  This project requires that each "
    msg += "commit have at least one 'Bugzilla:' entry.  Please add a 'Bugzilla: <bugzilla_URL>' "
    msg += "entry for each bugzilla."
    return msg


def closedbug_msg():  # pragma: no cover
    """Return the message to use as a note for BZs that are CLOSED."""
    msg = "This bug's status is CLOSED.  Please check that the Bugzilla information is correct."
    return msg


def nomrcve_msg():  # pragma: no cover
    """Return the message to note where bz_cve has found bug mr_cve wasn't."""
    msg = "This Bugzilla contains a CVE that was not listed in the merge request description. "
    msg += "Please verify the Bugzilla's URL is correct or add the CVE(s) to your MR description"
    msg += " as a 'CVE: CVE-YYYY-XXXXX' entry."
    return msg


def nobzcve_msg():  # pragma: no cover
    """Return the message to note where mr_cve has found bug bz_cve wasn't."""
    msg = "This MR description contains a CVE that was not found on any of the listed Bugzillas, "
    msg += "Please check the CVE(s) number(s) as well as its Bugzilla counterpart."
    return msg


def depbug_msg():  # pragma: no cover
    """Return the message to use as note for commits that are dependencies."""
    msg = "These commits are for dependencies listed in the MR description.  Your MR will not be "
    msg += "merged until these dependencies are in main."
    return msg


def nomrbug_msg():  # pragma: no cover
    """Return the message to note an unknown bug found in patches."""
    msg = "These commits contain a bugzilla number that was not listed in the merge request "
    msg += "description.  Please verify the bugzilla's URL is correct, or, add them to your MR "
    msg += "description as either a 'Bugzilla:' or a 'Depends:' entry."
    return msg


def bothbug_msg():  # pragma: no cover
    """Return the message to note a bug listed in both MR and dependencies."""
    msg = "This merge request contain a bugzilla number that was listed as being for both "
    msg += "this merge request and as a dependency. Re-use of bugs between merge requests, "
    msg += "particularly when dependent on one another, is not supported. Please sort out "
    msg += "which merge request this bugzilla number actually belongs to."
    return msg


def notapproved_msg():  # pragma: no cover
    """Return the message for bugs which are not approved."""
    msg = "The bugzilla associated with these commits is not approved at this time."
    return msg


def badinternalbug_msg():  # pragma: no cover
    """Return the message that 'INTERNAL' bug touches wrong source files."""
    msg = "These commits are marked as 'INTERNAL' but were found to touch "
    msg += "source files outside of the redhat/ directory. 'INTERNAL' bugs "
    msg += "are only to be used for changes to files in the redhat/ directory."
    return msg


def validate_internal_bz(commit_list):
    """Check file list of 'INTERNAL' bug only touch files under redhat/ or .gitlab*."""
    allowed_paths = ['redhat/', '.gitlab', '.get_maintainer.conf', 'makefile']
    allowed_paths += defs.RHDOCS_PATHS
    for commit in commit_list.values():
        for path in commit:
            if not path.startswith(tuple(allowed_paths)):
                return False
    return True


CVE_FIELDS = ['id',
              'short_desc',
              ]


def bug_is_verified(bug):
    """Confirm the bug is Verified."""
    if bug.product.startswith('Red Hat Enterprise Linux') \
            and bug.component in {'kernel', 'kernel-rt'} \
            and 'Tested' in bug.cf_verified:
        LOGGER.debug('Bug %s is Verified.', bug.id)
        return True
    LOGGER.debug('Bug %s is not Verified.', bug.id)
    return False


def find_cves_in_short_desc(bugs):
    """Return all CVE IDs at the start of the line."""
    cve_regex = re.compile(r'CVE-\d{4}-\d{4,7}(?=.*:)')
    cves = {}
    for bug in bugs:
        if cve := cve_regex.findall(bug.short_desc):
            cves[str(bug.id)] = cve
    return cves


def check_bzs_for_cve(bug_ids, bzcon):
    """Check if BZ is related to a CVE."""
    try:
        bugs = bzcon.getbugs(bug_ids, include_fields=CVE_FIELDS)
    except Fault:
        bugs = ""
    if not bugs:
        LOGGER.error("getbugs() did not return a useful result for BZ(s) %s.", bug_ids)
        return None

    if cves := find_cves_in_short_desc(bugs):
        for key, value in cves.items():
            LOGGER.debug('BZ: %s found CVEs: %s.', key, value)
        return cves

    LOGGER.debug('No CVEs found in BZ: %s.', bug_ids)
    return None


class BZState(enum.IntEnum):
    """Relevant BZ states for an MR."""

    INVALID = -3
    CLOSED = -2
    MISSING = -1
    NOT_READY = 0
    READY_FOR_QA = 1
    READY_FOR_MERGE = 2


def get_bug_data(bzcon, bug_list):
    """Query the list of bugs with bugzilla and return a dict of bug objects."""
    LOGGER.debug("Fetching bug data for bugs %s", bug_list)
    bugs = bzcon.getbugs(bug_list, include_fields=defs.BUG_FIELDS)
    return {str(bug.bug_id): bug for bug in bugs}


def set_invalid_bug_info(notes, bz_properties):
    """Fill in notes for invalid bugs."""
    noteid = 0
    while noteid < len(notes):
        if notes[noteid] == notapproved_msg():
            break
        noteid += 1
    if noteid == len(notes):
        notes.append(notapproved_msg())
    bz_properties['notes'].append(str(noteid+1))


def set_unfetched_bug_properties(valid_mr_bug, commits, bz_properties):
    """Set properties for a bug we didn't do a bugzilla lookup for."""
    if not valid_mr_bug:
        bz_properties['approved'] = BZState.NOT_READY
        bz_properties['logs'] = 'Not found in MR description'
    elif commits == {'DescOnly': []}:
        bz_properties['approved'] = BZState.NOT_READY
        bz_properties['logs'] = 'Not in any commits'
    else:
        bz_properties['approved'] = BZState.INVALID
        bz_properties['logs'] = 'Not a valid bug number'


def get_target_branch(project_id, target_branch):
    """Return the target Branch metadata object for the given target branch, or None."""
    branch = Projects().get_project_by_id(project_id).get_branch_by_name(target_branch)
    if not (policy := get_one_policy(branch.distgit_ref)):
        raise RuntimeError(f'Branch does not have a policy! {branch}')
    branch.set_policy(policy)
    return branch


def validate_bzs(project, merge_request, bugzilla_ids, bz_dependencies, cve_ids,  reviewed_items,
                 bzcon):
    # pylint: disable=too-many-locals,too-many-branches,too-many-statements,too-many-arguments
    """Validate bugzilla references."""
    target_branch = get_target_branch(project.id, merge_request.target_branch)
    suffix = '-alt' if '-alt-' in project.name else ''
    suffix = '-rt' if merge_request.target_branch.endswith('-rt') else suffix
    notes = []
    nobug_id = ""
    closedbug_id = ""
    depbug_id = ""
    nomrbug_id = ""
    nomrcve_id = ""
    nobzcve_id = ""
    notgt_id = ""
    bothbug_id = ""
    internalbug_id = ""

    bug_data = get_bug_data(bzcon, bugzilla_ids + bz_dependencies)

    for bug in reviewed_items:
        bz_properties = {'approved': BZState.NOT_READY, 'notes': [], 'logs': ''}
        reviewed_items[bug]["validation"] = bz_properties
        valid_mr_bug = True

        if not target_branch:
            notes += [] if notgt_id else ["Unknown repository target"]
            notgt_id = notgt_id if notgt_id else str(len(notes))
            bz_properties['notes'].append(notgt_id)
            continue

        if bug in bug_data and bug_data[bug].status == defs.BZ_STATE_CLOSED:
            notes += [] if closedbug_id else [closedbug_msg()]
            closedbug_id = closedbug_id if closedbug_id else str(len(notes))
            bz_properties['notes'].append(closedbug_id)
            bz_properties['approved'] = BZState.CLOSED
            continue

        if bug == 'INTERNAL':
            LOGGER.debug("Found an INTERNAL bug")
            if validate_internal_bz(reviewed_items[bug]['commits']):
                bz_properties['approved'] = BZState.READY_FOR_MERGE
            else:
                notes += [] if internalbug_id else [badinternalbug_msg()]
                internalbug_id = internalbug_id if internalbug_id else str(len(notes))
                bz_properties['notes'].append(internalbug_id)
            continue

        if bug == "-":
            notes += [] if nobug_id else [nobug_msg()]
            nobug_id = nobug_id if nobug_id else str(len(notes))
            bz_properties['notes'].append(nobug_id)
            bz_properties['approved'] = BZState.MISSING
            continue

        if bug == "+":
            LOGGER.debug("CVE %s not found in BZ But found on MR", reviewed_items[bug])
            notes += [] if nobzcve_id else [nobzcve_msg()]
            nobzcve_id = nobzcve_id if nobzcve_id else str(len(notes))
            bz_properties['notes'].append(nobzcve_id)
            continue

        if bug in bz_dependencies:
            if bug in bugzilla_ids:
                notes += [] if bothbug_id else [bothbug_msg()]
                bothbug_id = bothbug_id if bothbug_id else str(len(notes))
                bz_properties['notes'].append(bothbug_id)
            else:
                notes += [] if depbug_id else [depbug_msg()]
                depbug_id = depbug_id if depbug_id else str(len(notes))
                bz_properties['notes'].append(depbug_id)

        elif bug not in bugzilla_ids:
            notes += [] if nomrbug_id else [nomrbug_msg()]
            nomrbug_id = nomrbug_id if nomrbug_id else str(len(notes))
            bz_properties['notes'].append(nomrbug_id)
            valid_mr_bug = False

        if reviewed_items[bug]['cves']:
            if any(bz_cve not in cve_ids for bz_cve in reviewed_items[bug]['cves']):
                notes += [] if nomrcve_id else [nomrcve_msg()]
                nomrcve_id = nomrcve_id if nomrcve_id else str(len(notes))
                bz_properties['notes'].append(nomrcve_id)
                continue

        bug_ok = False
        verified = False
        if bug not in bug_data:
            set_unfetched_bug_properties(valid_mr_bug, reviewed_items[bug]['commits'],
                                         bz_properties)
        else:
            bug_ok, fail_reason = is_bug_ready(bug_data[bug], target_branch.policy)
            if fail_reason:
                bz_properties['logs'] = f'Bug {bug} has: {fail_reason}'
            verified = bug_is_verified(bug_data[bug])
            LOGGER.debug('Policy check: bug %s is %s', bug,
                         'Approved' if bug_ok else (f'Non-approved: {fail_reason}'))

        if valid_mr_bug and bug_ok:
            bz_properties['approved'] = BZState.READY_FOR_MERGE if verified \
                                        else BZState.READY_FOR_QA
        else:
            set_invalid_bug_info(notes, bz_properties)
    return notes, target_branch.distgit_ref


def get_report_table(reviewed_items):
    """Create the table for the bugzilla report."""
    mr_approved = BZState.READY_FOR_MERGE
    table = []
    for bug in reviewed_items:
        bz_properties = reviewed_items[bug]["validation"]
        mr_approved = min(mr_approved, bz_properties['approved'])
        cves = reviewed_items[bug]['cves']
        commits = []
        for commit in reviewed_items[bug]["commits"]:
            commits.append(commit)
        table.append([bug, commits, bz_properties['approved'].name, cves,
                      bz_properties['notes'], bz_properties['logs']])
    return table, mr_approved


def print_target(merge_request, bz_target):
    """Print the bugzilla target if MR targets main branch."""
    target = merge_request.target_branch
    target = target if target not in ("main", "main-rt") else f"{target} ({bz_target})"
    return "Target Branch: " + target


def print_gitlab_report(merge_request, notes, bz_target, table, mr_approved):
    """Print a bugzilla report for gitlab."""
    report = "<br>\n\n**Bugzilla Readiness "
    has_bugs = len(table) > 0
    desc_has_bugs_not_in_commits = False
    for row in table:
        if defs.BZ_IN_DESCRIPTION_ONLY in row[1]:
            desc_has_bugs_not_in_commits = True
    if mr_approved >= BZState.READY_FOR_QA and has_bugs and not desc_has_bugs_not_in_commits:
        report += "Report**\n\n"
    else:
        report += "Error(s)!**\n\n"
    report += print_target(merge_request, bz_target) + "   \n"
    report += " \n\n"
    table_header = "|BZ|Commits|Approved|CVEs|Details|Notes|\n"
    table_header += "|:------|:------|:------|:------|:------|:------|\n"
    table_entries = ""
    for row in table:
        bzn = f"BZ-{row[0]}" if row[0] not in ["-", "+"] else "-"
        commits = row[1] if len(row[1]) < 26 else row[1][:25] + ["(...)"]
        table_entries += "|"+bzn+"|"+"<br>".join(commits)
        table_entries += "|"+str(row[2])+"|"
        for cve in row[3]:
            table_entries += f"[{cve}](https://access.redhat.com/security/cve/{cve})<br>"
        table_entries += "|" + row[5]
        table_entries += "|"+common.build_note_string(row[4])

    footnotes = common.print_notes(notes)
    report += common.wrap_comment_table(table_header, table_entries, footnotes, "table")

    if mr_approved >= BZState.READY_FOR_QA and has_bugs:
        report += "\nMerge Request **passes** bugzilla validation\n"
    else:
        report += "\nMerge Request **fails** bugzilla validation.  \n"
        report += " \n"
        report += "Guidelines for these entries can be found in CommitRules, "
        report += "https://red.ht/kwf_commit_rules.  \n"
        report += "To request re-evalution after getting bugzilla approval "
        report += "add a comment to this MR with only the text: "
        report += "request-bz-evaluation  \n"
    if not has_bugs:
        report += "There were no valid bugs in the MR description or in any commits\n"
    if desc_has_bugs_not_in_commits:
        report += "There are bugs in the MR description not in any commits\n"

    return report


def build_review_lists(commit, review_lists, bug, cve=None):
    """Build review_lists for this bug."""
    found_files = []
    if bug == 'INTERNAL' and commit is not None:
        found_files = cdlib.extract_files(commit)

    bugd = review_lists[bug] if bug in review_lists else {}
    commits = bugd["commits"] if "commits" in bugd else {}
    cves = bugd["cves"] if "cves" in bugd else []

    if commit is not None:
        commits[commit.id] = found_files
    elif not cve:
        commits[defs.BZ_IN_DESCRIPTION_ONLY] = found_files

    if cve is not None:
        cves.extend(cve)
    bugd["commits"] = commits
    bugd["cves"] = cves
    return bugd


def check_on_bzs(project, merge_request, bugzilla_ids, bz_dependencies, cve_ids, bzcon):
    # pylint: disable=too-many-locals, too-many-arguments, too-many-branches
    """Check BZs."""
    review_lists = {}
    cmsg_ids = set()
    for commit in merge_request.commits():
        commit = project.commits.get(commit.id)
        # Skip merge commits, bugzilla link on the commit log for them
        # is not required and we do not care if bugs are listed on them
        if len(commit.parent_ids) > 1:
            continue
        try:
            found_bzs, non_mr_bzs, dep_bzs, _, _, _ = common.extract_all_from_message(
                commit.message, bugzilla_ids, cve_ids, bz_dependencies
            )
        # pylint: disable=broad-except
        except Exception:
            found_bzs = []
            non_mr_bzs = []
            dep_bzs = []

        if not found_bzs and cdlib.is_rhdocs_commit(commit):
            found_bzs = ["INTERNAL"]

        cmsg_ids |= set(found_bzs)

        for bug in found_bzs:
            review_lists[bug] = build_review_lists(commit, review_lists, bug)

        for bug in non_mr_bzs:
            review_lists[bug] = build_review_lists(commit, review_lists, bug)

        for bug in dep_bzs:
            review_lists[bug] = build_review_lists(commit, review_lists, bug)

        if not found_bzs and not non_mr_bzs and not dep_bzs:
            review_lists["-"] = build_review_lists(commit, review_lists, "-")

    if len(review_lists) == 0:
        for bug in bugzilla_ids:
            review_lists[bug] = build_review_lists(None, review_lists, bug)

    mrdesc_ids = set(bugzilla_ids)
    if cmsg_ids != mrdesc_ids:
        LOGGER.warning("Some bugs found in MR %d description not found in any commit(s)",
                       merge_request.iid)
        LOGGER.debug("Bugs found in MR %d description: %s",
                     merge_request.iid, sorted(mrdesc_ids))
        LOGGER.debug("Bugs found in MR %d commits: %s",
                     merge_request.iid, sorted(cmsg_ids))
        for bug in mrdesc_ids - cmsg_ids:
            review_lists[bug] = build_review_lists(None, review_lists, bug)
    else:
        LOGGER.debug("All MR %d description bugs present and accounted for in commit(s)",
                     merge_request.iid)

    if bugzilla_ids != ["INTERNAL"] and (cves := check_bzs_for_cve(bugzilla_ids, bzcon)):
        for bug, cve in cves.items():
            review_lists[bug] = build_review_lists(None, review_lists, bug, cve)

    for cve in cve_ids:
        LOGGER.debug("Found cve %s in MR %d description", cve, merge_request.iid)
        if all(cve not in value['cves'] for key, value in review_lists.items()):
            review_lists["+"] = build_review_lists(None, review_lists, "+", [cve])

    (notes, tgt) = validate_bzs(project, merge_request, bugzilla_ids, bz_dependencies, cve_ids,
                                review_lists, bzcon)
    (table, approved) = get_report_table(review_lists)

    return notes, tgt, table, approved


def bz_should_bail_early(instance, project, merge_request):
    """Check if we should exit early from processing."""
    hook_name = "bugzilla validation"
    commit_count, authlevel = common.get_commits_count(project, merge_request)
    if commit_count > defs.MAX_COMMITS_PER_MR and authlevel < MAINTAINER_ACCESS:
        msg = '**Bugzilla Readiness Error(s)**  \n'
        msg += '*ERROR*: This Merge Request has too many commits for the '
        msg += f'{hook_name} webhook to process ({commit_count}) -- please '
        msg += 'make sure you have targeted the correct branch.'
        common.update_webhook_comment(merge_request, instance.user.username,
                                      "Bugzilla Readiness", msg)
        return True

    return common.do_not_run_hook(project, merge_request, hook_name, True)


def bz_get_label_status(approved, bugzilla_ids):
    """Figure out the right status for the Bugzilla scoped label."""
    if approved is BZState.READY_FOR_MERGE:
        status = defs.READY_SUFFIX
    elif approved is BZState.READY_FOR_QA:
        status = defs.NEEDS_TESTING_SUFFIX
    elif approved is BZState.CLOSED:
        status = defs.BZ_STATE_CLOSED
    elif approved is BZState.INVALID:
        status = defs.BZ_NUMBER_INVALID
    elif not bugzilla_ids or approved is BZState.MISSING:
        status = defs.MISSING_SUFFIX
    else:
        status = defs.NEEDS_REVIEW_SUFFIX
    return status


def bz_post_report(instance, merge_request, notes, fmterrors, bz_target, table, approved):
    # pylint: disable=too-many-arguments
    """Publish results to gitlab."""
    report = print_gitlab_report(merge_request, notes, bz_target, table, approved)
    report += common.wrap_comment_table("", fmterrors, "", "error details")
    common.update_webhook_comment(merge_request, instance.user.username,
                                  "Bugzilla Readiness", report)


def run_bz_validation(instance, project, merge_request, code_changed):
    # pylint: disable=too-many-locals
    """Perform the Bugzilla validation for a merge request."""
    if bz_should_bail_early(instance, project, merge_request):
        return

    # If we do not connect to bugzilla then we cannot fully validate BZ readiness.
    bzcon = common.try_bugzilla_conn()
    if not bzcon:
        raise ConnectionError('Bugzilla API connection failed. Aborting.')
    bugzilla_ids, errors = common.extract_bzs(merge_request.description)
    cve_ids = common.extract_cves(merge_request.description)
    bz_dependencies, derrors = common.extract_dependencies(project, merge_request.description)
    LOGGER.info("Running BZ validation for merge request: iid=%s, pat_with_namespace=%s, "
                "description=%s, bz=%s, deps=%s, cve_ids=%s",
                merge_request.iid, project.path_with_namespace, merge_request.description,
                bugzilla_ids, bz_dependencies, cve_ids)

    (notes, bz_target, table, approved) = check_on_bzs(project, merge_request, bugzilla_ids,
                                                       bz_dependencies, cve_ids, bzcon)

    common.add_merge_request_to_milestone(instance, project, merge_request)

    if code_changed:
        common.set_bug_state(project, merge_request.iid, bugzilla_ids, bzcon, defs.BZ_STATE_POST)

    status = bz_get_label_status(approved, bugzilla_ids)

    fmterrors = errors + derrors
    if fmterrors:
        fmterrors = "\nBugzilla metadata formatting errors:  \n" + fmterrors
        status = defs.NEEDS_REVIEW_SUFFIX

    common.add_label_to_merge_request(instance, project, merge_request.iid,
                                      [f'Bugzilla::{status}'])
    mr_approved = approved >= BZState.READY_FOR_QA

    if mr_approved and not fmterrors:
        common.set_bug_state(project, merge_request.iid, bugzilla_ids, bzcon,
                             defs.BZ_STATE_MODIFIED)

    bz_post_report(instance, merge_request, notes, fmterrors, bz_target, table, approved)
    cdlib.set_dependencies_label(instance, project, merge_request)


def bz_process_umb(headers, **_):
    """Process a message sent by the umb_bridge."""
    namespace = headers['mrpath'].split('!')[0]
    mr_id = headers['mrpath'].split('!')[-1]
    url = f'https://gitlab.com/{namespace}/-/merge_requests/{mr_id}'

    gl_instance, project, merge_request, *_ = common.parse_mr_url(url)
    gl_instance.auth()  # we need to be authed if we need to post comments
    run_bz_validation(gl_instance, project, merge_request, False)


def bz_process_mr(gl_instance, message, **_):
    """Process a merge request message."""
    project = gl_instance.projects.get(message.payload["project"]["path_with_namespace"])
    if not (merge_request := common.get_mr(project, message.payload["object_attributes"]["iid"])):
        return
    code_changed = common.mr_action_affects_commits(message)
    run_bz_validation(gl_instance, project, merge_request, code_changed)


def bz_process_note(gl_instance, message, **_):
    """Process a note message."""
    LOGGER.debug("Checking note request\n")
    if "merge_request" not in message.payload:
        return

    force_eval = common.force_webhook_evaluation(message.payload['object_attributes']['note'], 'bz')
    if not force_eval:
        return

    project = gl_instance.projects.get(message.payload["project"]["path_with_namespace"])
    if not (merge_request := common.get_mr(project, message.payload["merge_request"]["iid"])):
        return
    run_bz_validation(gl_instance, project, merge_request, True)


WEBHOOKS = {
    "merge_request": bz_process_mr,
    "note": bz_process_note,
    defs.UMB_BRIDGE_MESSAGE_TYPE: bz_process_umb
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('BUGZILLA')
    args = parser.parse_args(args)
    common.generic_loop(args, WEBHOOKS)


if __name__ == "__main__":
    main(sys.argv[1:])
