"""Update bugs to contain links to an MR and successful pipeline artifacts."""
import re
import sys
from xmlrpc.client import Fault

from bugzilla import BugzillaError
from cki_lib import logger
from cki_lib import misc
from cki_lib.gitlab import get_instance
from gitlab.exceptions import GitlabGetError
from rcdefinition.rc_data import parse_config_data

from . import common
from . import defs
from . import pipelines

LOGGER = logger.get_logger('cki.webhook.buglinker')


def get_bugs(bzcon, bug_list):
    """Return a list of bug objects."""
    try:
        bz_results = bzcon.getbugs(bug_list)
        if not bz_results:
            LOGGER.info("getbugs() returned an empty list for these bugs: %s.", bug_list)
        return bz_results
    except BugzillaError:
        LOGGER.exception('Error getting bugs.')
        return None


def make_ext_bz_bug_id(namespace, mr_id):
    """Format the string needed for the tracker links."""
    return f"{namespace}/-/merge_requests/{mr_id}"


def update_bugzilla(bugs, namespace, mr_id, bzcon):
    """Filter input bug list and run bugzilla API actions."""
    ext_bz_bug_id = make_ext_bz_bug_id(namespace, mr_id)
    for action, bug_list in bugs.items():
        if not bug_list:
            continue
        # Ignore non-numeric bugs such as 'INTERNAL'. Bug list items are strings :/.
        filtered_bugs = [bug for bug in bug_list if bug.isdigit()]
        if filtered_bugs:
            if action == 'unlink':
                unlink_mr_from_bzs(filtered_bugs, ext_bz_bug_id, bzcon)
            else:
                bugs = get_bugs(bzcon, filtered_bugs)
                link_mr_to_bzs(bugs, ext_bz_bug_id, bzcon)


def bz_is_linked_to_mr(bug, ext_bz_bug_id):
    """Return the matching external tracker for the BZ, if any."""
    return next((tracker for tracker in bug.external_bugs if
                 tracker['type']['description'] == 'Gitlab' and
                 tracker['type']['url'] == defs.EXT_TYPE_URL and
                 tracker['ext_bz_bug_id'] == ext_bz_bug_id),
                None)


def unlink_mr_from_bzs(bugs, ext_bz_bug_id, bzcon):
    """Unlink the MR from the given list of BZs."""
    for bug in bugs:
        LOGGER.info('Unlinking %s from BZ%s.', ext_bz_bug_id, bug)
        if not misc.is_production():
            continue
        try:
            bzcon.remove_external_tracker(ext_type_description='Gitlab',
                                          ext_bz_bug_id=ext_bz_bug_id, bug_ids=bug)
        except BugzillaError:
            LOGGER.exception("Problem unlinking %s from BZ%s.", ext_bz_bug_id, bug)
        except Fault as err:
            if err.faultCode == 1006:
                LOGGER.warning('xmlrpc fault %d: %s', err.faultCode, err.faultString)
            else:
                raise


def get_kernel_bugs(bugs):
    """Filter out bugs which are not RHEL & a kernel component."""
    return [bug for bug in bugs if
            bug.product.startswith('Red Hat Enterprise Linux') and
            bug.component in ('kernel', 'kernel-rt')]


def _do_link_mr_to_bzs(bugs, ext_bz_bug_id, bzcon):
    if misc.is_production():
        try:
            bzcon.add_external_tracker(bugs, ext_type_url=defs.EXT_TYPE_URL,
                                       ext_bz_bug_id=ext_bz_bug_id)
        except BugzillaError:
            LOGGER.exception("Problem adding tracker %s to BZs.", ext_bz_bug_id)


def link_mr_to_bzs(bugs, ext_bz_bug_id, bzcon):
    """Take a list of bugs and a MR# and update the BZ's ET with the MR url."""
    untracked_bugs = [bug for bug in bugs if not bz_is_linked_to_mr(bug, ext_bz_bug_id)]
    if not untracked_bugs:
        LOGGER.info("All bugs have an existing link to %s.", ext_bz_bug_id)
        return

    kernel_bugs = get_kernel_bugs(untracked_bugs)
    bug_list = [bug.id for bug in kernel_bugs]

    LOGGER.info("Need to add %s to external tracker list of these bugs: %s", ext_bz_bug_id,
                bug_list)
    _do_link_mr_to_bzs(bug_list, ext_bz_bug_id, bzcon)


BASIC_BZ_FIELDS = ['component',
                   'external_bugs',
                   'id',
                   'product',
                   'summary'
                   ]

BASIC_BZ_QUERY = {'query_format': 'advanced',
                  'include_fields': BASIC_BZ_FIELDS,
                  'classification': 'Red Hat',
                  'component': 'kernel-rt'
                  }


def parse_cve_from_summary(summary):
    """Return a CVE ID from the string, or None."""
    result = re.match(r'^(\w* )?(?P<cve>CVE-\d{4}-\d{4,7})\s', summary)
    if result:
        return result.group('cve')
    return None


def get_rt_cve_bugs(bzcon, bug_list):
    """Identify whether bug_list BZs are CVEs and if so return matching kernel-rt bugs."""
    bz_results = get_bugs(bzcon, bug_list)
    cve_set = set()
    for bug in bz_results:
        cve_id = parse_cve_from_summary(bug.summary)
        if not cve_id:
            continue
        cve_set.add(cve_id)

    if not cve_set:
        return None

    cve_query = {**BASIC_BZ_QUERY}
    cve_query['product'] = bz_results[0].product
    # Blocked by the CVE
    cve_query['f1'] = 'blocked'
    cve_query['o1'] = 'anywords'
    cve_query['v1'] = list(cve_set)
    # Is not CLOSED DUPLICATE.
    cve_query['f2'] = 'resolution'
    cve_query['o2'] = 'notequals'
    cve_query['v2'] = 'DUPLICATE'
    # Is the same version.
    cve_query['f3'] = 'version'
    cve_query['o3'] = 'equals'
    cve_query['v3'] = bz_results[0].version

    try:
        return bzcon.query(cve_query)
    except BugzillaError:
        LOGGER.exception('Error querying bugzilla.')
        return None


def post_to_bugs(bug_list, post_text, pipeline_url, bridge_name, bzcon):
    """Submit post to given bugs."""
    filtered_bugs = []
    # If this is the RT pipeline and a CVE then we want to post to the kernel-rt bug.
    if pipelines.is_kernel_rt_pipeline(bridge_name):
        bugs = get_rt_cve_bugs(bzcon, bug_list)
    else:
        bugs = get_bugs(bzcon, bug_list)
    if not bugs:
        return None

    # It is possible for there to be multiple 'success' events for a given pipeline
    # so we should check that we haven't already posted them to the bugs. Sigh.
    for bug in bugs:
        if not comment_already_posted(bug, pipeline_url):
            filtered_bugs.append(bug.id)
    if not filtered_bugs:
        LOGGER.info('Pipeline results have already been posted to all relevant bugs.')
        return None

    if misc.is_production():
        try:
            comment = bzcon.build_update(comment=post_text, comment_private=True)
            bzcon.update_bugs(filtered_bugs, comment)
        except BugzillaError:
            LOGGER.exception('Error posting comments to bugs: %s', filtered_bugs)
            return None
    LOGGER.info('Posted comment to bugs: %s', filtered_bugs)
    return bugs


def comment_already_posted(bug, pipeline_url):
    """Return True if the pipeline results have already been posted to the given bug."""
    pipeline_text = f'Pipeline: {pipeline_url}'
    comments = bug.getcomments()
    for comment in comments:
        if comment['creator'] == defs.KERNEL_BZ_BOT and pipeline_text in comment['text']:
            LOGGER.info('Excluding bug %s as pipeline was already posted in comment %s.', bug.id,
                        comment['count'])
            return True
    return False


def create_bugzilla_text(title, mr_url, pipeline_url, repo_url, job_data):
    """Return formatted job data."""
    bz_post = 'The following Merge Request has pipeline job artifacts available:\n\n'
    bz_post += f'Title: {title}\nMR: {mr_url}\n'
    bz_post += f'Pipeline: {pipeline_url}\n\n'
    bz_post += ('This Repo URL is *not* accessible from a web browser!'
                ' It only functions as a dnf or yum baseurl.\n')

    bz_post += f'Repo URL: {repo_url}\n\n'

    for job in job_data:
        bz_post += '\n'
        bz_post += f"{job['version']}.{job['arch']}:\n"
        bz_post += f"Artifacts (RPMs): {job['browse_url']}\n"
        # Include test job details if we have them.
        if job['status']:
            bz_post += f"Test job: {job['job_url']}\n"

    bz_post += ("\nArtifacts expire after six weeks. If artifacts are needed after that time then"
                " please rerun the pipeline by visiting the MR's pipelines page and clicking the"
                " blue 'Run pipeline' button.\n")
    return bz_post


def get_artifact(job, path):
    """Return the given file from the job."""
    try:
        return job.artifact(path)
    except GitlabGetError:
        LOGGER.warning('Error getting path for job #%d: %s', job.id, path)
        return None


def parse_job_rc(job):
    """Return a dict with the arch, kernel version, and URLs from a job rc file."""
    # If the test job was cancled there will be no artifacts so no rc file to parse.
    if job.status == 'canceled':
        arch = job.name.split()[-1]
        version = None
        browse_url = None
        repo_url = None
        all_sources_targeted = False
    else:
        if not (rc_file := get_artifact(job, 'rc')):
            return {}

        rc_config = parse_config_data(rc_file)

        arch = rc_config['state'].get('kernel_arch')
        version = rc_config['state'].get('kernel_version')
        browse_url = rc_config['state'].get('kernel_browse_url')
        repo_url = rc_config['state'].get('kernel_package_url', '').replace(arch, '$basearch')
        all_sources_targeted = int(rc_config['state'].get('all_sources_targeted', 0))

    # The job status is set to None for non-test jobs (publish jobs)
    job_data = {'id': job.id,
                'arch': arch,
                'version': version,
                'browse_url': browse_url,
                'repo_url': repo_url,
                'job_url': job.web_url,
                'status': job.status if job.stage == 'test' else None,
                'all_sources_targeted': bool(all_sources_targeted)
                }

    return job_data


def get_pipeline_job_ids(pipeline, bridge_job):
    """Return the list of job IDs depending on the bridge_job name."""
    job_ids = {}
    jobs = pipeline.jobs.list(as_list=False)
    # The RT or SHADOW pipeline does not have test jobs so instead return its publish jobs.
    if pipelines.is_kernel_pipeline(bridge_job):
        job_ids = [job.id for job in jobs if job.stage == 'test']
    elif pipelines.is_kernel_rt_pipeline(bridge_job):
        job_ids = [job.id for job in jobs if job.stage == 'publish' and job.status == 'success']
    elif pipelines.is_kernel_shadow_pipeline(bridge_job):
        job_ids = [job.id for job in jobs if job.stage == 'setup' and job.status == 'success']
    return job_ids


def process_pipeline(msg, **_):
    """Parse pipeline or merge_request msg and run _process_pipeline."""
    if msg.payload['object_kind'] == 'pipeline':
        # Some pipeline events are not associated with an MR. Ignore them.
        if msg.payload['merge_request'] is None:
            LOGGER.info('Pipeline %s is not associated with an MR, ignoring.',
                        msg.payload['object_attributes']['id'])
            return
        mr_id = msg.payload["merge_request"]["iid"]
    elif msg.payload['object_kind'] == 'merge_request':
        mr_id = msg.payload["object_attributes"]["iid"]

    gl_instance = get_instance('https://gitlab.com')
    project = gl_instance.projects.get(msg.payload["project"]["path_with_namespace"])
    if not (merge_request := common.get_mr(project, mr_id)):
        return

    if merge_request.work_in_progress:
        LOGGER.info("MR %s is marked work in progress, ignoring.", mr_id)
        return
    if merge_request.head_pipeline is None:
        LOGGER.info("MR %s has not triggered any pipelines? head_pipeline is None.", mr_id)
        return

    pipeline_id = merge_request.head_pipeline.get('id')
    if not merge_request.head_pipeline.get('web_url', '').startswith('https://gitlab.com/redhat/'):
        LOGGER.info("MR %s head pipeline #%s is not in the Red Hat namespace: %s", mr_id,
                    pipeline_id, merge_request.head_pipeline.get('web_url'))
        return

    _process_pipeline(gl_instance, project, pipeline_id, merge_request)


def update_targeted_label(gl_instance, project, mr_id, jobs_data):
    """Ensure the 'missing targeted tests' label is on the MR as needed."""
    all_sources_targeted = [job for job in jobs_data if job['all_sources_targeted']]
    LOGGER.debug('%s out of %s jobs have targeted tests', len(all_sources_targeted),
                 len(jobs_data))
    # One set value is enough since the check is ran for a summary of all
    # supported arches
    if all_sources_targeted:
        LOGGER.info('Removing %s label.', defs.TARGETED_TESTING_LABEL)
        common.remove_labels_from_merge_request(project, mr_id, [defs.TARGETED_TESTING_LABEL])
    else:
        LOGGER.info('Adding %s label.', defs.TARGETED_TESTING_LABEL)
        common.add_label_to_merge_request(gl_instance, project, mr_id,
                                          [defs.TARGETED_TESTING_LABEL])


def pipeline_finished(status):
    """Return True if the pipeline is finished."""
    return status in ('success', 'failed')


def process_bridge_job(gl_instance, downstream_project, bridge_job):
    # pylint: disable=too-many-return-statements
    """Process the bridge job and return jobs_data."""
    if not pipelines.is_valid_bridge_job(bridge_job.name):
        LOGGER.info('Unrecognized bridge job: %s', bridge_job.name)
        return None

    LOGGER.info('Processing bridge job: %s', bridge_job.name)

    # Possibly the bridge job does not have a downstream pipeline yet.
    if not bridge_job.downstream_pipeline:
        LOGGER.info('Bridge job %s does not have a downstream pipeline.', bridge_job.name)
        return None

    # Assume the downstream project is the same for all bridge jobs...
    if not downstream_project:
        downstream_namespace = \
            common.parse_gl_project_path(bridge_job.downstream_pipeline['web_url'])
        downstream_project = gl_instance.projects.get(downstream_namespace)

    downstream_pipeline = downstream_project.pipelines.get(bridge_job.downstream_pipeline.get('id'))
    if not downstream_project or not downstream_pipeline:
        LOGGER.info('Could not get downstream pipeline for bridge job %s.', bridge_job.name)
        return None

    # If the pipeline isn't finished give up and wait for next time.
    if not pipeline_finished(downstream_pipeline.status):
        LOGGER.info('Pipeline %s is not finished: %s', downstream_pipeline.id,
                    downstream_pipeline.status)
        return None

    # Get the test job IDs or, for KERNEL_RT_PIPELINES, the *succesful* publish job IDs.
    job_ids = get_pipeline_job_ids(downstream_pipeline, bridge_job.name)
    if not job_ids:
        LOGGER.info('No successful publish jobs found for downstream pipeline %s in project %s.',
                    downstream_pipeline.id, downstream_project.name)
        return None

    # Process each job and for each one store necessary facts in a dict.
    jobs_data = []
    for job_id in job_ids:
        # "Job methods (play, cancel, and so on) are not available on ProjectPipelineJob objects.
        # To use these methods create a ProjectJob object."
        job_data = parse_job_rc(downstream_project.jobs.get(job_id))
        if job_data:
            jobs_data.append(job_data)

    # If we don't have any artifacts or all the jobs were canceled then we have nothing
    # useful to post.
    if not [job for job in jobs_data if job['status'] != 'canceled']:
        LOGGER.info('No artifact data; nothing to post.')
        return None

    return jobs_data


def _process_pipeline(gl_instance, project, pipeline_id, merge_request):
    # pylint: disable=too-many-locals
    """Process a successful pipeline event message."""
    # If the MR doesn't have any bugs listed, then we have nothing to do.
    bug_list, _ = common.extract_bzs(merge_request.description)
    if not bug_list:
        LOGGER.info('No bugs found in MR %s description.', merge_request.iid)
        return

    local_pipeline = project.pipelines.get(pipeline_id)
    bzcon = None

    # Get data from all jobs.
    has_shadow = False
    has_build_done = False
    all_jobs = {}
    for bridge_job in local_pipeline.bridges.list():
        has_shadow = has_shadow or pipelines.is_kernel_shadow_pipeline(bridge_job.name)
        jobs_data = process_bridge_job(gl_instance, None, bridge_job)
        if not jobs_data:
            LOGGER.debug('No job data.')
            continue
        all_jobs[bridge_job.name] = jobs_data
        has_build_done = has_build_done or pipelines.is_kernel_pipeline(bridge_job.name)

    # Process each of the expected bridge jobs separately.
    for bridge_job, jobs_data in all_jobs.items():
        # We want to post shadow builds when they exist, skip non-shadow ones.
        if pipelines.is_kernel_pipeline(bridge_job) and has_shadow:
            LOGGER.debug('Skipping pipeline job data for build with shadow.')
            continue
        # When there is a shadow build, make sure we succeeded with main build.
        if pipelines.is_kernel_shadow_pipeline(bridge_job) and not has_build_done:
            LOGGER.debug('We have shadow build but without main build/test.')
            continue

        # We need a bugzilla connection to be able to just post.
        if not bzcon:
            bzcon = common.try_bugzilla_conn()
            if not bzcon:
                LOGGER.error('No bugzilla connection.')
                continue

        pipeline_url = f'https://gitlab.com/{project.path_with_namespace}/-/pipelines/{pipeline_id}'
        repo_url = next(job['repo_url'] for job in jobs_data if job['repo_url'])
        bug_post_text = create_bugzilla_text(merge_request.title, merge_request.web_url,
                                             pipeline_url, repo_url, jobs_data)
        LOGGER.info('Generated comment text:\n%s', bug_post_text)
        bugs = post_to_bugs(bug_list, bug_post_text, pipeline_url, bridge_job, bzcon)

        # For kernel-rt we separately add external tracker links to those CVE BZs.
        if pipelines.is_kernel_rt_pipeline(bridge_job) and bugs:
            ext_bz_bug_id = make_ext_bz_bug_id(project.path_with_namespace, merge_request.iid)
            link_mr_to_bzs(bugs, ext_bz_bug_id, bzcon)

        # For the regular kernel and centos-stream shadow pipelines, we may add a label
        # if there are not any targeted tests.
        if pipelines.is_kernel_pipeline(bridge_job) or \
           pipelines.is_kernel_shadow_pipeline(bridge_job):
            update_targeted_label(gl_instance, project, merge_request.iid, jobs_data)


def process_mr(msg, **_):
    """Process a merge request event message."""
    namespace = msg.payload['project']['path_with_namespace']
    mr_id = msg.payload["object_attributes"]["iid"]
    action = msg.payload['object_attributes'].get('action')
    LOGGER.info('Processing event for MR %s!%s (%s)', namespace, mr_id, action)

    is_draft, draft_changed = common.draft_status(msg.payload)
    # Has the Draft flag been removed? Then run the pipeline linker.
    if not is_draft and draft_changed:
        LOGGER.info('Sending MR event to pipeline processor.')
        process_pipeline(msg)

    # If the event is not for a new or closing MR and does not correspond to some change in the MR
    # description or draft status then we can ignore it.
    if action not in ('open', 'close', 'reopen') and 'description' not in \
       msg.payload.get('changes') and not draft_changed:
        LOGGER.info('MR has not changed, ignoring event.')
        return

    # If the MR is a Draft we want to ignore it unless it just flipped to Draft state then
    # trick bugs_to_process() into giving us all the bugs to unlink.
    if is_draft:
        if draft_changed:
            LOGGER.info('MR changed to Draft, unlinking bugs.')
            action = 'close'
        else:
            LOGGER.info('MR is a Draft, ignoring.')
            return

    mr_desc = msg.payload['object_attributes'].get('description')
    if not mr_desc:
        gl_instance = msg.gl_instance()
        project = gl_instance.projects.get(namespace)
        merge_request = common.get_mr(project, mr_id)
        mr_desc = merge_request.description if merge_request else None
    bugs = common.bugs_to_process(mr_desc, action, msg.payload['changes'])
    if not bugs['link'] and not bugs['unlink']:
        LOGGER.info('No relevant bugs found in MR description.')
        return

    if not (bzcon := common.try_bugzilla_conn()):
        return

    update_bugzilla(bugs, namespace, mr_id, bzcon)


WEBHOOKS = {
    "merge_request": process_mr,
    'pipeline': process_pipeline,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('BUGLINKER')
    args = parser.parse_args(args)
    common.generic_loop(args, WEBHOOKS, get_gl_instance=False)


if __name__ == "__main__":
    main(sys.argv[1:])
