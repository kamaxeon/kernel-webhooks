"""Common helpers for graphql."""
from functools import reduce
from operator import getitem

from cki_lib.gitlab import get_token
from cki_lib.logger import get_logger
from cki_lib.misc import is_production
from cki_lib.session import get_session
from gql import Client
from gql import gql
from gql.transport.exceptions import TransportQueryError
from gql.transport.requests import RequestsHTTPTransport

LOGGER = get_logger('cki.webhook.graphql')

# Query for GitlabGraph.create_note()
CREATE_NOTE_MUTATION = gql("""
mutation createNote($globalId: ID!, $body: String!) {
  createNote(input: {noteableId: $globalId, body: $body}) {
    note {
      id
    }
  }
}
""")


def _check_user(results, check_user):
    """Return True if the query currentUser username matches the check_user."""
    return check_user == results['currentUser']['username']


def _check_keys(results, check_keys):
    """Return True if all the keys are in the given query results."""
    return check_keys <= results.keys()


def check_query_results(results, check_keys, check_user):
    """Perform some optional checks of query results."""
    # See GitlabGraph.execute_query().
    # Ignore our own messages (for bots). Query must include 'currentUser' field.
    if check_user and _check_user(results, check_user):
        LOGGER.info('Ignoring message from %s.', check_user)
        return None

    # Raise an error if not all expected keys are in the results.
    if check_keys and not _check_keys(results, check_keys):
        raise RuntimeError(f'Gitlab did not return all keys {check_keys} in {results}.')
    return results


class GitlabGraph:
    """A wrapper object for interacting with gitlab graphql."""

    def __init__(self, headers=None, session=None, fetch_schema=False):
        """Set up the client."""
        self.client = None
        self.connect(headers=headers, session=session, fetch_schema=fetch_schema)

    class _CkiRequestsHTTPTransport(RequestsHTTPTransport):
        """A RequestsHTTPTransport with a cki_lib session object used by our connect() method."""

        # pylint: disable=too-few-public-methods
        def __init__(self, *args, session=None, headers=None, **kwargs):
            super().__init__(*args, **kwargs)
            self.session = session or get_session(__name__)
            self.session.headers.update(headers or {})

        def connect(self) -> None:
            """Skip connect as we have our own session."""

        def close(self) -> None:
            """Skip close as we have our own session."""

    def connect(self, headers=None, session=None, fetch_schema=False):
        """Connect to graphql using cki_lib get_token()."""
        if not self.client:
            if not headers:
                headers = {}

            if not session and (token := get_token('https://gitlab.com')) is not None:
                headers['Authorization'] = f'Bearer {token}'

            transport = self._CkiRequestsHTTPTransport(session=session, headers=headers,
                                                       url='https://gitlab.com/api/graphql')
            self.client = Client(transport=transport, fetch_schema_from_transport=fetch_schema)
        LOGGER.info('Connected.')

    def execute_query(self, query, params=None, check_keys=None, check_user=None):
        """Execute the query with the given params.

        Query may include:
        - currentUser field with username subfield to use check_user

        Parameters:
        query (gql.gql): The query to run.
        params (dict): Query parameters, if any.
        check_keys (set): Key names required to be in the query result, if any. Will raise
                          a RuntimeError if not all keys are found.
        check_user (str): Returns None if the query result currentUser['username'] matches
                          check_user, if any. Useful to see if we are running as a bot.

        Returns:
        dict:dict returned by gql.client.execute() or None if optional checks fail.
        """
        LOGGER.debug('Executing query with params: %s', params)
        try:
            results = self.client.execute(query, variable_values=params)
        except TransportQueryError as err:
            LOGGER.error('Encountered %d error(s) executing query: %s', len(err.errors),
                         query.loc.source.body)
            for count, error in enumerate(err.errors, start=1):
                LOGGER.error('%d: %s', count, error)
            raise
        return check_query_results(results, check_keys, check_user)

    def execute_paged_query(self, query, page_key, params=None, check_keys=None, check_user=None):
        # pylint: disable=too-many-arguments
        """Execute a query with fields that require pagination.

        See webhook.signoff.SIGNOFF_QUERY for example usage.

        Query must include:
        - $cursor variable used as after: value of the field with paged results
        - pageInfo subfield with hasNextPage and endCursor in the field with paged results
        - $first_pass variable used to @include all fields which are _not_ paged
        - (optional) currentUser field with username subfield to use check_user

        Parameters:
        query (gql.gql): The query to run.
        page_key (list): Path to location in query results where PageInfo will be.
        params (dict): Query parameters, if any.
        check_keys (set): Key names required to be in the query result, if any. Will raise
                          a RuntimeError if not all keys are found.
        check_user (str): Returns None if the query result currentUser['username'] matches
                          check_user, if any. Useful to see if we are running as a bot.

        Returns:
        dict:dict returned by gql.client.execute() with page_key['nodes'] packed or None if optional
             checks fail.
        """
        LOGGER.debug('Running paged queries with params: %s, page_key: %s', params, page_key)
        cursor = ''
        results = []  # first query is stored here and subsequent query results are appended
        while cursor is not None:
            params['cursor'] = cursor
            params['first_pass'] = not bool(cursor)
            # No need to check these after the first time
            if not params['first_pass']:
                check_keys = None
                check_user = None
            new_results = self.execute_query(query, params=params, check_keys=check_keys,
                                             check_user=check_user)
            if not new_results:
                return new_results
            # If all the expected results are not populated (ie. due to unknown MR ID) then
            # getitem() raises a TypeError. Just return what we got and give up?
            try:
                paged_data = reduce(getitem, page_key, new_results)
            except TypeError:
                LOGGER.warning('page_key %s not found in query results %s', page_key, new_results)
                return new_results

            if params['first_pass']:
                results = new_results
            else:
                # not our first pass so 'extend' existing results with new results
                reduce(getitem, page_key, results)['nodes'].extend(paged_data['nodes'])

            cursor = paged_data['pageInfo']['endCursor'] if \
                paged_data['pageInfo']['hasNextPage'] else None
        return results

    def create_note(self, global_id, body):
        """Create a note if in prod. Returns the new note's global ID on success, otherwise None."""
        LOGGER.info('Leaving note for %s: %s', global_id, body)
        if not is_production():
            return True
        params = {'globalId': global_id, 'body': body}
        result = self.execute_query(CREATE_NOTE_MUTATION, params, check_keys={'createNote'})
        if not result:
            raise RuntimeError('Note mutation did not return expected results.')
        # pylint: disable=unsubscriptable-object
        return result['createNote']['note']['id']
