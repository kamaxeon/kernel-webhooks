"""Event handlers."""
import copy
import sys

from cki_lib import cki_pipeline
from cki_lib import config_tree
from cki_lib.logger import get_logger
from cki_lib.messagequeue import Message
import cki_lib.misc
import gitlab
from yaml import Loader
from yaml import load

from . import common

LOGGER = get_logger('cki.webhook.external_ci')
CI_CONFIG = {}

EXTERNAL_LABEL_NAME = 'External Contribution'
LIMITED_LABEL_NAME = 'Limited CI'


EMOJI_MAP = {
    'created': ':hourglass_flowing_sand:',
    'pending': ':hourglass_flowing_sand:',
    'running': ':hourglass_flowing_sand:',
    'canceled': ':grey_exclamation:',
    'skipped': ':grey_exclamation:',
    'success': ':heavy_check_mark:',
    'failed': ':sob:',
    'manual': ':no_entry:'
}

WELCOME_MESSAGE = """Hi! This is the friendly CKI test bot.

It appears that you are not a member of {group}. This means that the automatic
CI pipeline on your MR will fail. As getting testing is important, I'll be
responsible for testing your changes. After every MR change, I'll start a
small testing pipeline and link it here so you can follow the results.

**Reviewers can start a full CI run by going into the "Pipelines" tab on this
MR and clicking the "Run pipeline" button.** A passing full run is required in
order to integrate your changes.

You can find more details about the contributor setup, permission issues and
how to resolve them in the [CKI documentation](https://cki-project.org/docs/user_docs/\
gitlab-mr-testing/full_picture#trusted-and-external-contributors).
"""


PIPELINE_MESSAGE = """Limited testing pipeline:

&nbsp;&nbsp;{link} - {status} {emoji}


"""


def already_commented(merge_request, bot_name, members_of):
    """Check if bot_name user already added a welcome comment on the MR.

    Args:
        merge_request: MR object as returned by the gitlab module
        bot_name:      Username of the bot account to check.
        members_of:    Group or project to fill into the message template.

    Returns:
        True if a note added by the bot exists on the MR
        False if there is no note added by the bot on the MR
    """
    return any(note.attributes['author']['username'] == bot_name and
               note.attributes['body'].strip() == WELCOME_MESSAGE.format(group=members_of).strip()
               for note in merge_request.notes.list(as_list=False))


def get_vars_from_hook(var_list):
    """Get a dictionary of variables from pipeline webhook.

    The webhook data contains a list of dictionaries which is less fun to deal
    with than a regular key/value dictionary.
    """
    return {v['key']: v['value'] for v in var_list}


def handle_mr(gitlab_instance, project_config, mr_data):
    """Trigger pipelines for given MR."""
    mr_id = mr_data['object_attributes']['iid']
    labels = []

    # Get extra data
    path_with_ns = mr_data['project']['path_with_namespace']
    iid = mr_data['object_attributes']['iid']
    extra_variables = {
        'git_url': mr_data['object_attributes']['target']['git_http_url'],
        'branch': mr_data['object_attributes']['target_branch'],
        'commit_hash': mr_data['object_attributes']['last_commit']['id'],
        'name': mr_data['project']['path_with_namespace'],
        'title': f"{path_with_ns}: MR {iid}",
        'mr_id': mr_id,
        'origin_path': mr_data['project']['path_with_namespace'],
        'mr_url': mr_data['object_attributes']['url']
    }

    project = gitlab_instance.projects.get(project_config['.mr_project'])
    if not (merge_request := common.get_mr(project, mr_id)):
        return
    if not already_commented(merge_request,
                             gitlab_instance.user.username,
                             project_config['.members_of']):
        # If this is the first time we see the MR that means there is no
        # label assigned to it yet.
        labels.append(EXTERNAL_LABEL_NAME)
        if cki_lib.misc.is_production():
            merge_request.notes.create(
                {'body': WELCOME_MESSAGE.format(group=project_config['.members_of'])}
            )
        else:
            LOGGER.info('Not a production deployment, nod adding comments or label')

    trigger = copy.deepcopy(project_config)
    trigger.update(extra_variables)
    trigger = config_tree.clean_config(trigger)

    status_message = 'Testing pipeline status:\n'

    if '.cki_external_pipeline_branch' not in project_config or \
            '.cki_external_pipeline_project' not in project_config:
        LOGGER.info('No pipeline project or branch configured for %s',
                    mr_data['project']['path_with_namespace'])
        common.add_label_to_merge_request(gitlab_instance, project, mr_id, labels)
        return

    trigger['cki_pipeline_branch'] = project_config['.cki_external_pipeline_branch']
    trigger['cki_project'] = project_config['.cki_external_pipeline_project']
    trigger['cki_pipeline_type'] = 'external'
    external_pipeline = cki_pipeline.trigger_multiple(gitlab_instance, [trigger])[0]

    status_message = ''.join([
        status_message,
        PIPELINE_MESSAGE.format(
            link=external_pipeline.attributes['web_url'],
            status='created',
            emoji=EMOJI_MAP['created'])
    ])
    labels.append(LIMITED_LABEL_NAME + '::running')
    common.add_label_to_merge_request(
        gitlab_instance, project, mr_id, labels, remove_scoped=True
    )

    if cki_lib.misc.is_production():
        merge_request.notes.create({'body': status_message})
    else:
        LOGGER.info('Not a production deployment, not posting comments')
        LOGGER.info('STATUS COMMENT:\n%s', status_message)


def get_stage_suffix(job_info_list):
    """Get the ::{stage} suffix of the non-passing job of a pipeline (or '')."""
    failed_job = next((job for job in job_info_list if job['status'] == 'failed'), None)
    return f'::{failed_job["stage"]}' if failed_job else ''


def update_pipeline_data(gl_instance, msg, **_):
    # pylint: disable=too-many-locals,too-many-branches
    """Update existing MR with current pipeline status."""
    pipeline_id = str(msg.payload['object_attributes']['id'])
    pipeline_vars = get_vars_from_hook(
        msg.payload['object_attributes']['variables']
    )

    # Is this a pipeline we care about?
    project_path = pipeline_vars.get('origin_path')
    for project_config in CI_CONFIG.values():
        if project_path == project_config['.mr_project']:
            project = gl_instance.projects.get(project_path)
            break
    else:
        LOGGER.info('No matching MR project for %s, found %s',
                    msg.payload['commit']['url'],
                    project_path)
        return

    pipeline_status = msg.payload['object_attributes']['status']
    is_retriggered = cki_lib.misc.strtobool(pipeline_vars.get('retrigger', 'False'))
    if is_retriggered:
        LOGGER.info('Handling a retriggered pipeline, not posting comments')
        LOGGER.info('STATUS:\n%s', PIPELINE_MESSAGE.format(
            link=f"{msg.payload['project']['web_url']}/pipelines/{pipeline_id}",
            status=pipeline_status,
            emoji=EMOJI_MAP[pipeline_status]
        ))
        return

    mr_id = pipeline_vars['mr_id']
    if not (merge_request := common.get_mr(project, mr_id)):
        return
    bot_name = gl_instance.user.username

    # Find the correct comment to edit. Bot only creates simple notes so we
    # don't need to dig through every comment in discussions and it's enough to
    # check the top comments.
    bot_discussions = [discussion for discussion in merge_request.discussions.list(as_list=False)
                       if discussion.attributes['notes'][0]['author']['username'] == bot_name]
    for index, discussion in enumerate(bot_discussions):
        first_comment_data = discussion.attributes['notes'][0]
        if pipeline_id in first_comment_data['body']:
            if index + 1 == len(bot_discussions):
                # Only update the labels if this is the newest change set

                # The label should say OK instead of success to be consistent with other labels.
                label_status = 'OK' if pipeline_status == 'success' else pipeline_status
                details = label_status + get_stage_suffix(msg.payload['builds'])

                common.add_label_to_merge_request(
                    gl_instance, project, mr_id, [f'{LIMITED_LABEL_NAME}::{details}'],
                    remove_scoped=True
                )
            comment = discussion.notes.get(first_comment_data['id'])
            break
    else:
        # Did someone delete our original comment?
        LOGGER.error('Unable to find comment for %s in MR %s from %s',
                     msg.payload['commit']['url'],
                     mr_id,
                     project_path)
        return

    # Edit and update the comment
    body_lines = comment.body.splitlines()
    for index, line in enumerate(body_lines):
        if pipeline_id in line:
            new_line = line.rsplit('-', maxsplit=1)[0] + \
                f' - {pipeline_status} {EMOJI_MAP[pipeline_status]}'
            body_lines[index] = new_line
            break
    if cki_lib.misc.is_production():
        comment.body = '\n'.join(body_lines)
        comment.save()
    else:
        LOGGER.info('Not a production deployment, not posting comments')
        LOGGER.info('UPDATED COMMENT:\n%s', '\n'.join(body_lines))


def load_config(config_path):
    """Load webhook configuration.

    Read the config yaml, parse out the actual definitions and populate
    CI_CONFIG.
    """
    # Calm down pylint, there's no better way to pass along global settings if
    # process_message doesn't take extra args
    global CI_CONFIG  # pylint: disable=global-statement
    with open(config_path, encoding='utf-8') as config_file:
        unparsed_config = load(config_file, Loader=Loader)

    CI_CONFIG = config_tree.process_config_tree(unparsed_config)


def get_project_config(project_name):
    """Get the configuration for given project."""
    for project_config in CI_CONFIG.values():
        if project_config['.mr_project'] == project_name:
            return project_config

    LOGGER.error('Missing config for %s', project_name)
    return {}


def get_manual_hook_data(gitlab_instance, mr_object, project_path):
    """Retrieve MR data that mimic the webhook so handler can be reused."""
    project = gitlab_instance.projects.get(project_path)

    return Message({
        'object_attributes': {
            'target': {'git_http_url': project.attributes['http_url_to_repo']},
            'target_branch': mr_object.attributes['target_branch'],
            'iid': mr_object.attributes['iid'],
            'last_commit': {'id': mr_object.attributes['sha']},
            'url': mr_object.web_url
        },
        'project': {'name': project.attributes['name'],
                    'path_with_namespace': project_path},
        'user': {'username': mr_object.attributes['author']['username']}
    })


def process_mr_message(gl_instance, message, **_):
    """Check everything about the MR message and call the handler."""
    # pylint: disable=unused-argument
    project_name = message.payload['project']['path_with_namespace']

    project_config = get_project_config(project_name)
    if not project_config:
        return

    author = message.payload['user']['username']
    try:
        group = gl_instance.groups.get(project_config['.members_of'])
    except gitlab.GitlabGetError:
        group = gl_instance.projects.get(project_config['.members_of'])

    for member in group.members_all.list(as_list=False):
        if member.attributes['username'] == author:
            LOGGER.debug('Found internal contributor, not triggering')
            return

    if not common.mr_action_affects_commits(message):
        LOGGER.debug('Not a code change, ignoring')
        return

    # If we got this far we should trigger the pipeline(s)
    handle_mr(gl_instance, project_config, message.payload)


WEBHOOKS = {
    'merge_request': process_mr_message,
    'pipeline': update_pipeline_data,
}


def main(args):
    """Set up and start consuming messages or handle manual requests."""
    load_config(cki_lib.misc.get_env_var_or_raise('CONFIG_PATH'))

    parser = common.get_arg_parser('EXTERNAL_CI')
    args = parser.parse_args(args)
    if args.merge_request:
        gitlab_instance, _, mr_object, project_path = common.parse_mr_url(
            args.merge_request
        )
        gitlab_instance.auth()
        manual_hook_data = get_manual_hook_data(gitlab_instance, mr_object, project_path)
        process_mr_message(gitlab_instance, manual_hook_data)
        return

    # Always run with the bot user check disabled
    args.disable_user_check = True

    common.consume_queue_messages(args, WEBHOOKS)


if __name__ == '__main__':
    main(sys.argv[1:])
