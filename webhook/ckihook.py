"""Manage the CKI labels."""
from dataclasses import InitVar
from dataclasses import dataclass
from dataclasses import field
from enum import IntEnum
from enum import auto
import sys

from cki_lib import logger
from cki_lib.gitlab import get_instance
from cki_lib.misc import strtobool
from gql import gql

from webhook import common
from webhook import defs
from webhook import fragments
from webhook.graphql import GitlabGraph
from webhook.pipelines import PipelineType
from webhook.pipelines import get_pipeline_type

LOGGER = logger.get_logger('cki.webhook.ckihook')


class PipelineStatus(IntEnum):
    """Possible status of a pipeline."""

    UNKNOWN = auto()
    MISSING = auto()
    FAILED = auto()
    CANCELED = auto()
    RUNNING = auto()
    PENDING = RUNNING
    CREATED = RUNNING
    OK = auto()
    SUCCESS = OK

    def title(self):
        """Return capitalized name."""
        return self.name.capitalize() if self.name != 'OK' else 'OK'


@dataclass(frozen=True)
class PipelineResult:
    """Basic pipeline details.

    Given a `name`, `status`, and optional `failed_stage`, we set `label` and `type`.
    or
    Given a graphql downstream pipeline node dict, we set all other public values.
    """

    name: str = ''
    status: PipelineStatus = PipelineStatus.UNKNOWN
    label: str = field(init=False, default='')
    type: PipelineType = field(init=False, default=PipelineType.INVALID)
    failed_stage: InitVar(str) = ''
    api_dict: InitVar(dict) = {}

    def __post_init__(self, failed_stage, api_dict):
        """Parse the api_dict, if any."""
        if api_dict:
            self.__dict__['name'] = api_dict['sourceJob']['name']
            self.__dict__['status'] = get_pipeline_status(api_dict['status'])
            failed_stage = api_dict['jobs']['nodes'][0]['stage']['name'] if self.status is \
                PipelineStatus.FAILED else ''
        self.__dict__['type'] = get_pipeline_type(self.name)
        self._set_label(failed_stage)
        LOGGER.debug('Created %s', self)

    def _set_label(self, failed_stage):
        """Set the label for pipelines of a known `type`."""
        prefix = ''
        if self.type in (PipelineType.KERNEL, PipelineType.KERNEL_LEGACY):
            prefix = defs.CKI_KERNEL_PREFIX
        elif self.type in (PipelineType.KERNEL_RT, PipelineType.KERNEL_RT_LEGACY):
            prefix = defs.CKI_KERNEL_RT_PREFIX
        if not prefix:
            return
        self.__dict__['label'] = f'{prefix}::{self.status.title()}::{failed_stage}' if \
                                 failed_stage else f'{prefix}::{self.status.title()}'


PIPELINE_QUERY_CHECK_KEYS = {'currentUser', 'project'}
PIPELINE_BASE = """
query mrData($namespace: ID!, $mr_id: String!) {
  ...CurrentUser
  project(fullPath: $namespace) {
    mergeRequest(iid: $mr_id) {
      headPipeline {
        downstream {
          nodes {
            ...CkiPipeline
          }
        }
      }
    }
  }
}
"""

PIPELINE_QUERY = gql(PIPELINE_BASE + fragments.CURRENT_USER + fragments.CKI_PIPELINE)


class ComputeCKI(GitlabGraph):
    """Class to fetch an MR's downstream pipeline status."""

    def __init__(self, event_user, namespace, mr_id):
        """Store basic properties."""
        GitlabGraph.__init__(self)
        self.event_user = event_user  # user that triggered the webhook event
        self.namespace = namespace
        self.mr_id = mr_id

    def run_query(self):
        """Query the MR and its pipelines."""
        LOGGER.info('Computing CKI labels for %s!%s.', self.namespace, self.mr_id)
        query_params = {'namespace': self.namespace,
                        'mr_id': str(self.mr_id),
                        }
        results = self.execute_query(PIPELINE_QUERY, query_params,
                                     check_keys=PIPELINE_QUERY_CHECK_KEYS,
                                     check_user=self.event_user)
        return parse_pipeline_query_results(results)


def parse_pipeline_query_results(results):
    """Check PIPELINE_QUERY query results and return a list of PipelineResult objects."""
    if not results:
        LOGGER.info('Nothing to parse.')
        return []
    # The MR value will be None if it doesn´t exist.
    if not results['project'].get('mergeRequest'):
        LOGGER.warning('Merge request does not exist, ignoring.')
        return []
    # If the headPipeline or downstream data is not available then we have nothing to do.
    if not (head_pipeline := results['project']['mergeRequest'].get('headPipeline')):
        LOGGER.warning('MR does not have headPipeline set.')
        return []
    if not head_pipeline['downstream']['nodes']:
        LOGGER.warning('headPipeline does not have any downstream nodes set.')
        return []
    return list(map(lambda p: PipelineResult(api_dict=p), head_pipeline['downstream']['nodes']))


def get_failed_stage(status, stages, jobs):
    """Return the name of the failed stage if status is FAILED, otherwise None."""
    if status is not PipelineStatus.FAILED:
        return None
    for stage in reversed(stages):
        if next((job for job in jobs if job['stage'] == stage and job['status'] == 'failed'),
                False):
            return stage
    return None


def get_pipeline_status(status):
    """Return the PipelineStatus whose name matches the input, or PipelineStatus.UNKNOWN."""
    return next((member for name, member in PipelineStatus.__members__.items()
                 if name == status.upper()), PipelineStatus.UNKNOWN)


def parse_mr_url(url):
    """Return MR namespace and ID parsed from a gitlab url."""
    mr_id = int(url.split('/')[-1])
    namespace = url.removeprefix('https://gitlab.com/').removesuffix(f'/-/merge_requests/{mr_id}')
    return namespace, mr_id


def add_labels(namespace, mr_id, labels):
    """Set the given labels."""
    LOGGER.info('Setting labels %s', labels)
    gl_instance = get_instance('https://gitlab.com')
    gl_project = gl_instance.projects.get(namespace)
    common.add_label_to_merge_request(gl_instance, gl_project, mr_id, labels, remove_scoped=True)


def compute_new_labels(event_user, namespace, mr_id):
    """Return a new list of CKI labels for the given MR."""
    results = ComputeCKI(event_user, namespace, mr_id).run_query()
    new_labels = [pipe.label for pipe in results if pipe.label]
    # Return our new labels but if any are missing append ::Missing ones as well
    return new_labels + check_for_missing_labels(new_labels) if new_labels else []


def cki_label_changed(changes):
    """Return True if any CKI label changed, or False."""
    return (common.has_label_prefix_changed(changes, f'{defs.CKI_KERNEL_PREFIX}::') or
            common.has_label_prefix_changed(changes, f'{defs.CKI_KERNEL_RT_PREFIX}::'))


def check_for_missing_labels(label_list):
    """Return a list of CKI[_RT]::Missing labels if the input label list does not have them."""
    missing_labels = []
    for prefix in (defs.CKI_KERNEL_PREFIX, defs.CKI_KERNEL_RT_PREFIX):
        if not [label for label in label_list if label.startswith(f'{prefix}::')]:
            missing_labels.append(f'{prefix}::Missing')
    return missing_labels


def process_mr_event(msg, **_):
    """Process an MR message."""
    mr_id = msg.payload['object_attributes']['iid']
    namespace = msg.payload['project']['path_with_namespace']
    event_user = msg.payload['user']['username']
    LOGGER.info('Processing MR event for %s!%s from %s.', namespace, mr_id, event_user)

    # If someone changed the CKI labels then recalculate them.
    if cki_label_changed(msg.payload['changes']):
        LOGGER.info('CKI labels changed. Calculating new ones.')
        if labels_to_add := compute_new_labels(event_user, namespace, mr_id):
            add_labels(namespace, mr_id, labels_to_add)
        return
    # or if the MR doesn't have CKI labels add ::Missing ones.
    if labels_to_add := check_for_missing_labels([lbl['title'] for lbl in msg.payload['labels']]):
        LOGGER.info('MR is missing one or more CKI labels.')
        add_labels(namespace, mr_id, labels_to_add)
        return
    LOGGER.info('No CKI label changes to make.')
    return


def process_note_event(msg, **_):
    """Process a note message."""
    if not common.force_webhook_evaluation(msg.payload['object_attributes']['note'], 'cki'):
        LOGGER.info('Note event did not request evaluation, ignoring.')
        return
    mr_id = msg.payload['merge_request']['iid']
    namespace = msg.payload['project']['path_with_namespace']
    event_user = msg.payload['user']['username']
    LOGGER.info('Processing note event for %s!%s from %s.', namespace, mr_id, event_user)
    if labels_to_add := compute_new_labels(event_user, namespace, mr_id):
        add_labels(namespace, mr_id, labels_to_add)
        return
    LOGGER.info('No CKI label changes to make.')
    return


def process_pipeline_event(msg, **_):
    """Process a pipeline message."""
    # This should filter out upstream pipeline events and any downstream pipeline events which are
    # not directly related to an MR.
    if not (mr_url := common.get_pipeline_variable(msg.payload, 'mr_url')) or \
       ((retrigger := common.get_pipeline_variable(msg.payload, 'retrigger')) and
           strtobool(retrigger)):
        LOGGER.info('Event did not contain the expected variables, ignoring')
        return

    namespace, mr_id = parse_mr_url(mr_url)
    name = common.get_pipeline_variable(msg.payload, 'trigger_job_name')
    status = get_pipeline_status(msg.payload['object_attributes']['status'])
    failed_stage = get_failed_stage(status, msg.payload['object_attributes']['stages'],
                                    msg.payload['builds'])

    # If the status is FAILED but there is no failed_stage then just assume someone retriggered a
    # job and set status to RUNNING 😬.
    if status is PipelineStatus.FAILED and failed_stage is None:
        status = PipelineStatus.RUNNING

    LOGGER.info('Processing pipeline event (id: %s, name: %s , status: %s) for %s!%s.',
                msg.payload['object_attributes']['id'], name, status.name.title(), namespace, mr_id)

    pipeline = PipelineResult(name=name, status=status, failed_stage=failed_stage)
    if pipeline.label:
        LOGGER.info('Setting label %s.', pipeline.label)
        add_labels(namespace, mr_id, [pipeline.label])
    else:
        LOGGER.info('No label to set for this pipeline.')
    return


WEBHOOKS = {
    'merge_request': process_mr_event,
    'note': process_note_event,
    'pipeline': process_pipeline_event,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('CKIHOOK')
    args = parser.parse_args(args)
    common.generic_loop(args, WEBHOOKS, get_gl_instance=False)


if __name__ == "__main__":
    main(sys.argv[1:])
