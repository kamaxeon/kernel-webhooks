"""GraphQL Fragments to use with queries.

See https://graphql.org/learn/queries/#fragments

"""

CKI_PIPELINE = """
fragment CkiPipeline on Pipeline {
  status
  sourceJob {
    name
  }
  jobs(statuses: [FAILED]) {
    nodes {
      name
      status
      stage {
        name
      }
    }
  }
}
"""

CURRENT_USER = """
fragment CurrentUser on Query {
  currentUser {
    username
  }
}
"""

MR_COMMITS = """
fragment MrCommits on MergeRequest {
  commits(after: $cursor) {
    pageInfo {
      hasNextPage
      endCursor
    }
    nodes {
      sha
      description
    }
  }
}
"""

MR_FILES = """
fragment MrFiles on MergeRequest {
  files: diffStats {
    path
  }
}
"""

MR_LABELS = """
fragment MrLabels on MergeRequest {
  labels {
    nodes {
      title
    }
  }
}
"""
