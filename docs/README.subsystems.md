# Subsystems Webhook

## Purpose

This webhook identifies the subsystems that a Merge Request's changeset modifies and adds a "Subsystem" specific label.

## Notifications

This webhook will post a notification report with the subscribing people tagged to relevant Merge Requests.  Any updates to those subscriptions will be delivered via edits to that same comment.   For more information about Subsystem subscribers, visit the [kernel-watch project](https://gitlab.com/redhat/rhel/src/kernel/watch.git)

## Manual Runs

You can run the webhook manually on a merge request URL with the command:

    python3 -m webhook.subsystems \
        --disable-inactive-branch-check \
        --merge-request https://gitlab.com/group/repo/-/merge_requests/1 \
        --owners-yaml owners.yml \
	--local-repo-path /data/kernel-watch \
	--kernel-watch-url https://gitlab.com/ptalbert/kernel-watch

The [main README](README.md#running-a-webhook-for-one-merge-request) describes
some common environment variables that can be set that are applicable for all
webhooks.

The `--map-file` argument can also be passed to the script via the `MAP_FILE`
environment variable, `--local-repo-path` with `LOCAL_REPO_PATH`, and
'--kernel-watch-url` with KERNEL_WATCH_URL.
