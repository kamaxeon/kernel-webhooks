# Signoff Webhook

## Notifications

This webhook will report the status of the DCO signoff of a Merge Request in a comment.  Any updates to that status will be delievered via edits to that same comment.

## Manual Runs

You can run the webhook manually on a merge request URL with the command:

    python3 -m webhook.signoff \
        --disable-inactive-branch-check \
        --merge-request https://gitlab.com/group/repo/-/merge_requests/1 \

The [main README][1] describes
some common environment variables that can be set that are applicable for all
webhooks.

## Verbose Logging

This webhook manages the `Signoff` scoped label on the merge request. By
default, a summary table is not logged if the status is OK. Additional
logging can be enabled by leaving a comment on the merge request that starts
with either `request-evaluation` or `request-signoff-evaluation`. The former
command will request evaluation from most webhooks.

## Purpose

This hook enforces [CommitRules][2] by labeling an MR with a 'Signoff' scoped
label indicating whether the MR's *commits* and *description* all have a valid
[DCO signoff][3] line.

[1]: README.md#running-a-webhook-for-one-merge-request
[2]: https://docs.google.com/document/d/15Y8Io4N2gtvt1nn5WzfFtssiKz5vbS7YoBy81XMg9S0/#heading=h.s7aphn448iz1
[3]: https://developercertificate.org
