#!/usr/bin/env python
"""Check for open Mrs with resolved threads, and validates if it was resolved by a valid user."""

import argparse
import os
import sys

from cki_lib import logger
from cki_lib import misc
from gitlab.const import MAINTAINER_ACCESS
from gitlab.const import NO_ACCESS
from gql import gql

from webhook import common
from webhook import defs

LOGGER = logger.get_logger('utils.acks_blocked_check')

OPEN_MRS = """
query OpenMrs($namespace: ID!, $cursor: String!) {
  project(fullPath: $namespace) {
    mergeRequests(state: opened, after: $cursor) {
      pageInfo {
        hasNextPage
        endCursor
      }
      nodes {
        id
        iid
      }
    }
  }
}
"""

DISCUSSION_RESOLVER_QUERY = """
query DiscussionResolver($namespace: ID!, $mr_iid: String!, $cursor: String!) {
  project(fullPath: $namespace) {
    mergeRequest(iid: $mr_iid) {
      discussions(after: $cursor) {
        pageInfo {
          hasNextPage
          endCursor
        }
        nodes {
          id
          resolved
          notes {
            nodes {
              url
              author {
                username
              }
            }
          }
          resolvedBy {
            username
          }
        }
      }
    }
  }
}
"""

USER_PERMISSION = """
query ProjectMember($namespace: ID!, $username: String!) {
  project(fullPath: $namespace) {
    projectMembers(search: $username) {
      nodes {
        user {
          username
        }
        accessLevel {
          integerValue
        }
      }
    }
  }
}
"""

DISCUSSION_ACTION = """
mutation comment($noteableID: NoteableID!, $discussionID: DiscussionID!, $body: String! ) {
  createNote(input: {body: $body, noteableId: $noteableID, discussionId: $discussionID}) {
    errors
  }
  discussionToggleResolve(input: {id: $discussionID, resolve: false}) {
    errors
  }
}
"""


def improperly_resolved_msg(resolver, author):  # pragma: no cover
    """Return the message to use as a note for commits without bz."""
    msg = f"This discussion has been re-opened as it was improperly resolved by @{resolver}, "
    msg += "discussions must be resolved by a thread participant or a maintainer.\n"
    msg += f"@{author} consider reviewing this discussion and resolving it, if you see fit"
    return msg


def get_mrs_with_discussions(client, namespace):
    """Return a list of the MR IDs with their resolved discussions."""
    open_mrs = {}
    mr_list = []
    count = 0

    # Get open MRS
    cursor = ''
    while cursor is not None:
        query_params = {'namespace': namespace, 'cursor': cursor}
        result = do_query(client, gql(OPEN_MRS), query_params, 'project')

        mr_data = result['project'].get('mergeRequests')
        cursor = mr_data['pageInfo']['endCursor'] if mr_data['pageInfo']['hasNextPage'] else None

        for merge_request in mr_data['nodes']:
            open_mrs[merge_request['iid']] = merge_request["id"]

    # Filter by MRs with resolved discussions
    for mr_iid, mr_id in open_mrs.items():
        cursor = ''
        discussion_list = []

        while cursor is not None:
            query_params = {'namespace': namespace, 'mr_iid': mr_iid, 'cursor': cursor}

            result = do_query(client, gql(DISCUSSION_RESOLVER_QUERY), query_params, 'project')

            discussion_data = result['project'].get('mergeRequest')['discussions']
            cursor = discussion_data['pageInfo']['endCursor'] \
                if discussion_data['pageInfo']['hasNextPage'] else None
            count += len(discussion_data['nodes'])

            for discussion in discussion_data['nodes']:
                if discussion['resolved']:
                    discussion_list.append(discussion)

        if discussion_list:
            mr_list.append({mr_id: discussion_list})

    LOGGER.info('Project %s found %s discussions, with %s being resolved.', namespace, count,
                len(mr_list))

    return mr_list


def get_user_permission(client, namespace, username):
    """Return the user access level to the given project, or NO_ACCESS if not a member."""
    query_params = {'namespace': namespace, 'username': username}

    result = do_query(client, gql(USER_PERMISSION), query_params, 'project')
    if (searched_users := result['project']['projectMembers']['nodes']):
        if access_level := next((node['accessLevel']['integerValue'] for node in
                                searched_users if node['user']['username'] == username), NO_ACCESS):
            return access_level
    LOGGER.error('Could not fetch %s permission to project %s.', username, namespace)
    return NO_ACCESS


def do_query(client, query, values, expected_key):
    """Do a graphql query."""
    result = client.execute(query, variable_values=values)
    if not result[expected_key]:
        LOGGER.error('Gitlab did not return the expected data: %s', result)
        sys.exit(1)
    return result


def _get_parser_args():
    parser = argparse.ArgumentParser(description='Check for improperly resolved threads')

    # Global options
    parser.add_argument('-p', '--projects', default=os.environ.get('GL_PROJECTS', '').split(),
                        help='gitlab projects to fetch MRs from.', nargs='+', required=False)
    return parser.parse_args()


def add_comment_to_discussion(client, noteableid, discussionid, body):
    """Add comment to gitlab MR discussion and change it resolved state."""
    query_params = {"noteableID": noteableid, "discussionID": discussionid, "body": body}

    result = do_query(client, gql(DISCUSSION_ACTION), query_params, 'createNote')

    if result['createNote']['errors'] != [] or result['discussionToggleResolve']['errors'] != []:
        LOGGER.error('Could not add comment to discussion, createNote: %s, '
                     'discussionToggleResolve: %s.', result['createNote']['errors'],
                     result['discussionToggleResolve']['errors'])
        sys.exit(1)


def main():
    # pylint: disable=too-many-nested-blocks
    """Find MRs opened with resolved threads."""
    args = _get_parser_args()
    LOGGER.info('Finding MRs with resolved thread...')

    client = common.gl_graphql_client()
    for project in args.projects:
        LOGGER.info("Validating Project %s", project)
        merge_requests = get_mrs_with_discussions(client, project)

        if not merge_requests:
            LOGGER.info('All MRs from %s with resolved discussions have been validated,'
                        ' nothing to do.', project)
            continue

        for merge_request in merge_requests:
            for mr_id, discussions in merge_request.items():
                for discussion in discussions:
                    resolved_by = discussion['resolvedBy']['username']

                    # Check if user is somehow involved with the thread or if the bot already
                    # commented on the thread
                    if all(author['author']['username'] not in (resolved_by, *defs.BOT_ACCOUNTS)
                           for author in discussion['notes']['nodes']):
                        # Check if the user who resolved the thread is maintainer or higher
                        if get_user_permission(client, project, resolved_by) < MAINTAINER_ACCESS:
                            author = discussion['notes']['nodes'][0]['author']['username']
                            # Un-resolve discussion and notify author
                            if misc.is_production():
                                add_comment_to_discussion(client, mr_id, discussion['id'],
                                                          improperly_resolved_msg(resolved_by,
                                                                                  author))
                            LOGGER.info("Improperly resolved thread, user: %s thread: %s",
                                        discussion['resolvedBy']['username'],
                                        discussion['notes']['nodes'][0]['url'])
                    else:
                        LOGGER.info("User %s Match author or involved with discussion %s",
                                    discussion['resolvedBy']['username'],
                                    discussion['notes']['nodes'][0]['url'])


if __name__ == '__main__':
    main()
